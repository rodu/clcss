% meanC_year
% computes yearly average of CG tensor, instead of monthly averages 
% which are done in meanC. 
% Revise line 14 to read your velocity; units should be m/s, it will 
% be converted to km/day.
% Check velocity dimensions in lines 24--25, should be Ny x Nx x Nt
% to compute CLCS see compute_squeeze_lines.
lb2
disp(['starting meanC_year ' datestr(now)])
lb2
T2=-7;% -7 is the value for the T described in paper, computation is probably not sensitive to this value.  
% choose directory where you would like to save your results, for now it uses pwd 
dirr=[pwd,filesep];    
load('hycom_clim','u','v','lon','lat') %load climatological velocity with variables: lon lat u v time 
assert(isvector(lon) & isvector(lat),'lon and lat should be vectors, not arrays')

    if iscolumn(lon) %lon lat must be row vectors
    lon=lon.';
    end
    if iscolumn(lat)
        lat=lat.';
    end
    
    u=permute(u,[2 1 3]); %vel should have dimensions Ny x Nx x Nt; units should be m/s, it will be converted to km/day below.
    v=permute(v,[2 1 3]); %vel should have dimensions Ny x Nx x Nt; units should be m/s, it will be converted to km/day below.


%prepare boundary conditions
numBC=3;
u(1:numBC,:,:)=0;v(1:numBC,:,:)=0; %south
u(end-numBC:end,:,:)=0;v(end-numBC:end,:,:)=0;%north

u(:,1:numBC,:)=0;v(:,1:numBC,:)=0; %west
u(:,end-numBC:end,:)=0;v(:,end-numBC:end,:)=0;%east


lon=double(lon);lat=double(lat); %hycom writes lon lat in single precision, need double.

if ~exist('time','var')
    time=1:360; % appropriate time for 360-day climatology
end
dt=diff(time);% units should be day
assert(max(abs(diff(dt)))<1e-6,'Error dt is not uniform, please use constant dt')% if time is single precision and dt is uniform, the difference between dts should not be greater than 1e-6, i think --- might need to tweek this
dataperday=1/dt(1);
assert(round(dataperday)==dataperday,'dataperday should be an integer')
ddt=dt(1);

ticyear=tic;

dirr2=[dirr,'CG',filesep,'fullyear'];
mkdir(dirr2)

Tvec1=(T2:-2:-360)-1;

count_year=0;
lda2year=0;
sqrtlda2year=0;
ftleyear=0;
C11year=0;
C22year=0;
C12year=0;
for Tind=1:length(Tvec1)
    T=Tvec1(Tind);
    iniday=abs(T); 
    numdays=abs(T2)*dataperday;
    timeindex=((iniday+T2+ddt)*dataperday:dataperday:(iniday)*dataperday);
    tspan=time(timeindex); 
    disp('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    disp(['= = = = = = = = =          New iniday ',pad(num2str(iniday),3),' --- month ',pad(num2str(monthnum),2),...
        '        = = = = = = = = ='])
    disp('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')

    disp(datestr(tspan))
    umonth=u(:,:,timeindex);
    vmonth=v(:,:,timeindex);
 
    iu=isnan(umonth);
    iv=isnan(vmonth);
    umonth(iu)=0;
    umonth(iv)=0;
    vmonth(iu)=0;
    vmonth(iv)=0;
    
    umonth=umonth*86.4; % m/s --> km/day
    vmonth=vmonth*86.4;whi
    lb2

    ticC=tic;
    [C11,C22,C12,x0,y0,lonx0,latx0]=compute_C(lon,lat,umonth,vmonth,T2,tspan,dirr2);
    %     [C11,C22,C12,x0,y0,lonx0,latx0]=compute_C_par(lon,lat,umonth,vmonth,T2,tspan,dirr2);

% % FTLE, rho, C, lambda_2 and their averaging. 
detC = C11.*C22 - C12.^2;
trC = C11 + C22;
lda2 = real(.5*trC + sqrt(.25*trC.^2 - detC));
ftle=log(lda2)/(2*abs(T2));

lda2year=lda2+lda2year;
sqrtlda2year=sqrtlda2year+sqrt(lda2);
ftleyear=ftleyear+ftle;
C11year=C11year+C11;
C22year=C22year+C22;
C12year=C12year+C12;
count_year=count_year+1;

end %ini day loop

lb2
disp(['Finished averaging CG, total time for full year was ',num2str(toc(ticyear)/60),' minutes'])
lb2

note='to get average do e.g. sqrtlda2year/count_year which should be the same as sqrtlda2year/length(Tvec1)';
save([dirr2,filesep,'yearly-averaged-CG']...
    ,'lonx0','latx0','lda2year','sqrtlda2year','T2','ftleyear','C11year',...
    'C22year','C12year','x0','y0','note','Tvec1','count_year')
disp('just saved')
disp([dirr2,filesep,'yearly-averaged-CG'])
disp('with variables')
whos('lonx0','latx0','lda2year','sqrtlda2year','T2','ftleyear','C11year',...
    'C22year','C12year','x0','y0','note','Tvec1','count_year')


% comment if you don't want to plot
figure
pcolor(lonx0, latx0, log(sqrtlda2year/count_year))
shading flat
colormap shadden
caxis([0.3 1.3])
colorbar
title('yearly average')
drawnow

lb2
disp(['finished meanC_year ' datestr(now)])
lb2
