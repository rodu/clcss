function hh=colorline(x, y, c,lw)
% colorline trajectory color plot.
%  colorline(X,Y,C,lw) colors parametric curve (X,Y)(s) according to C(s). 
% with linewidth lw, default is lw=1;
if nargin < 4
    lw=1;
end
if isrow(x)
   x=x.'; 
end

if isrow(y)
   y=y.'; 
end

if isrow(c)
   c=c.'; 
end

% % this option is slightly slower, might be better in some cases? ============
% hh=surface([x x], [y y], [c c], ...
% 	'FaceColor', 'none',        ...
% 	'EdgeColor', 'interp',      ...
% 	'Marker', 'none','Linewidth',lw);

% % this option is slightly faster                                 ============
x(end+1)=nan;
y(end+1)=nan;
c(end+1)=nan;
nn=nan(size(x));
hh=patch([x,nn],[y,nn],[c,nn],'FaceColor','none','EdgeColor','interp','LineWidth',lw);


% uistack(hh,'bottom')
end
