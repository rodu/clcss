function F_div(monthvec)
% F_div(monthvec)
% F_div computes the parameter alpha as defined in Appendix A of
% Duran et al 2018 for months in monthvec.
% alpha is the fractional change of area due todivergence,
% alpha it is the Jacobian determinant of the flow map
% when alpha is nearly one, shrinking and expandining of area is negligible
%
% For all twelve months use C_div(1:12) this produces monthly-averages
% and a yearly average at the same time, without much overhead.
% Revise line 24 to read your velocity; units should be m/s, it will
% be converted to km/day.
% Check velocity-array dimensions in lines 30--31, should be Ny x Nx x Nt
%
disp('================================================')
disp(['starting F_div ' datestr(now)])
disp('================================================')
T2vec=-7;% -7 is the value for the T described in paper, use same as used for compute_C
ticyear=tic;

% choose directory where you would like to save your results, for now it uses pwd
dirr=[pwd,filesep];

load('hycom_clim','u','v','lon','lat') %load climatological velocity with variables: lon lat u v and time if available
%dirr_file='F:\data\OceanVelocity\Climatologies\';
% filename='HYCOM15_FIL_MEAN_360.nc';
%filename='HYCOM00_FIL_MEAN_360.nc';
%[u,v,lon,lat,time]=read_hycom_vel_traj([dirr_file,filename]);

if nargin==0
    monthvec=1:12;
end
% lon lat =====================
assert(isvector(lon) & isvector(lat),'lon and lat should be vectors, not arrays')
if iscolumn(lon) %lon lat must be row vectors
    lon=lon.';
end
if iscolumn(lat)
    lat=lat.';
end
lon=double(lon);lat=double(lat); %hycom writes lon lat in single precision, need double.
% lon lat =====================


% ==================================== GRID ====================================
% Initial conditions grid for trajectories, use same as in compute_C to
% replicate flow map used for cLCS
% The higher the value of N the greater the resolution, 1024 is good for
% the Gulf of Mexico which is roughly 1200x1600 km
% N = 1024;
% % N=N*2; % this doubles the resolution
% % it will obviously be slower to compute.
% fac = (x0max-x0min)/abs(y0max-y0min);
% if fac > 1 % wider rather than tall
%     Nx0 = N;
%     Ny0 = fix(N/fac);
% else % either taller or square
%     Ny0 = N;
%     Nx0 = fix(N*fac);
% end

% 2 points in computational grid for every velocity grid point, use the
% same number here that you used in compute_C or compute_C_par
Nx0=numel(lon)*2;
Ny0=numel(lat)*2;

disp(['======= Grid of initial conditions for Flow map is '...
    ,num2str(Ny0),' by ',num2str(Nx0),' ======='])

lon0min = min(lon(:)); % [deg]
lon0max = max(lon(:)); % [deg]
lat0min = min(lat(:)); % [deg]
lat0max = max(lat(:)); % [deg]

lon_origin=lon0min; lat_origin=lat0min;
[x0min, y0min] = sph2xy(lon0min, lon_origin, lat0min, lat_origin);
[x0max, y0max] = sph2xy(lon0max, lon_origin, lat0max, lat_origin);
x0min = x0min*1e-3;   % [km]
y0min = y0min*1e-3;   % [km]
x0max = x0max*1e-3;   % [km]
y0max = y0max*1e-3;   % [km]

[xspan, yspan] = sph2xy(lon, lon_origin, lat, lat_origin);
xspan=xspan*1e-3; %[km]
yspan=yspan*1e-3; %[km]

% Nxy0 = Nx0*Ny0;
x0 = linspace(x0min, x0max, Nx0);
y0 = linspace(y0min, y0max, Ny0);
[lonx0,latx0]=xy2sph(x0*1e3, lon_origin, y0*1e3, lat_origin);
[X0, Y0] = meshgrid(x0, y0);
N=length(x0);
M=length(y0);
% ==================================== GRID ====================================

% vel and divergence =====================
u=permute(u,[2 1 3]); %vel should have dimensions Ny x Nx x Nt, this is true for
% divergence computation as well
v=permute(v,[2 1 3]); %vel should have dimensions Ny x Nx x Nt
u=u*86.4; % m/s --> km/day
v=v*86.4;

x=double(cumsum(xspan));% xspan is in km
y=double(cumsum(yspan))';
for kk=size(u,3):-1:1 % compute divergence [1/day]
    divt(:,:,kk)=divergence(x,y,u(:,:,kk),v(:,:,kk));% divergence is Matlab built in function, also needs dimensions Ny x Nx
end
%prepare boundary conditions
numBC=3;
u(1:numBC,:,:)=0;v(1:numBC,:,:)=0; %south
u(end-numBC:end,:,:)=0;v(end-numBC:end,:,:)=0;%north

u(:,1:numBC,:)=0;v(:,1:numBC,:)=0; %west
u(:,end-numBC:end,:)=0;v(:,end-numBC:end,:)=0;%east

divt(:,1:numBC,:)=0; %trajectories get stuck here anyway so div is not meaningful
divt(1:numBC,:,:)=0;
divt(end-numBC:end,:,:)=0;
divt(:,end-numBC:end,:)=0;
% vel and divergence =====================

% time =====================
if ~exist('time','var')
    dtt=1; % appropriate for one value daily, use dt=1/2 for 2 daily values
    time=1:dtt:size(u,3);
end
dt=diff(time);% units should be day
assert(max(abs(diff(dt)))<1e-6,'Error dt is not uniform, please use constant dt')% if time is single precision and dt is uniform, the difference between dts should not be greater than 1e-6, i think --- might need to tweek this
dataperday=round(1/dt(1));
% ddt=dt(1);
ddt=1; %one data per day, integrations will use proper dt
% time =====================
dirr2=[dirr,'alongpath_div',filesep];
mkdir(dirr2)
% begin alpha computation =====================
for monthnum=monthvec
    ticmonth=tic;
    % modify directory if desired

    % for T2=T2vec %fnow fixed at T2vec=-7;
    T2=T2vec;
    Tvec1=(T2:-2:-30);
    Tvec=Tvec1-((monthnum-1)*30);%same length as Tvec1
    %     expintdivTOT2=0;
    %     expintdivMonths2.(['month_',num2str(monthnum)])=nan(N*M,length(Tvec));% 1024*798 comes from N=length(x0);M=length(y0); below

    expintdivTOT=0;
    expintdivMonths.(['month_',num2str(monthnum)])=nan(N*M,length(Tvec));% 1024*798 comes from N=length(x0);M=length(y0); below

    for Tind=1:length(Tvec)

        T=Tvec(Tind);
        iniday=abs(T);
        %         numdays=abs(T2)*dataperday;
        timeindex=((iniday+T2+ddt)*dataperday:1:(iniday)*dataperday);
        tspan=time(timeindex)';%+datenum('1900-12-31','yyyy-mm-dd'); %convert to HyCOM format i.e. days since 1900-12-31
        disp(['New iniday ',num2str(iniday),' -- month ',num2str(monthnum),' = = = = = = = = = = = = = = = = = ='])
        disp(datestr(tspan))
        umonth=u(:,:,timeindex);
        vmonth=v(:,:,timeindex);
        divmonth=divt(:,:,timeindex);

        inan=isnan(umonth+vmonth);
        umonth(inan)=0;
        vmonth(inan)=0;
        divmonth(inan)=0;

        % Compute flow map ==============
        ticC=tic;
        if isrow(tspan)% should be a column
            tspan=tspan.';
        end
        %         t=flipud(tspan);
        %         t2 = tspan(end):sign(T)*.2:tspan(1);
        [x,y,t]=div_traj(umonth,vmonth,xspan,yspan,tspan,X0,Y0,tspan);%the latter tspan is requesting solution at same times as velocity
        disp(['Total time for trajectories was: minutes = ',num2str(toc(ticC)/60)])
        % Compute flow map ==============

        % Divergence along flow map ==============
        Nt=length(t);
        d=nan(Ny0,Nx0,Nt);
        % next is along-path divergence
        disp(' ========= interpolate along path now =========')
        for kk=0:Nt-1
            tic
            divxy2 = interp2(xspan,yspan,divmonth(:,:,end-kk),x(:,kk+1),y(:,kk+1),'linear') ;
            % divmonth is Ny x Nx x Nt with tspan
            % x,y are Nx*Ny x Nt with t=fliplr(tspan)
            toc
            d(:,:,kk+1)=reshape(divxy2,Ny0,Nx0);%d=divergence(x(t1),y(t1),t1) with t1=t(kk+1)=tspan(end-kk);
        end

        % Divergence along flow map ==========================================
        % I suggest making sure both methods agree:
        % integration can be done with a simple sum
        %         dt=-1; %minus one day
        %         expintdiv2=exp(nansum(d,3)*dt);%dt=-1day
        %         expintdivTOT2=expintdiv2+expintdivTOT2; %sum for latter average.
        %         expintdivMonths2.(['month_',num2str(monthnum)])(:,Tind)=expintdiv2(:);

        % or integration can be done with trapezoidal method (preferred)
        d2=d;
        iinan=isnan(d);
        d2(iinan)=0;
        int=trapz(t,d2,3);
        %         int(iinan)=NaN;
        expintdiv=exp(int);
        expintdivTOT=expintdiv+expintdivTOT; %sum for latter average.
        expintdivMonths.(['month_',num2str(monthnum)])(:,Tind)=expintdiv(:);
        % Divergence along flow map ==========================================

        %         % compare divergence computations if desired
        %         figure
        %         pcolor(X0,Y0,100*(1-expintdiv2))
        %         shading flat
        %         cmocean balance
        %         axis image
        %         caxis([-5 5])
        %         colorbar
        %         title('Percentage change in area trapezoidal')
        %
        %         figure
        %         pcolor(X0,Y0,100*(1-expintdiv2))
        %         shading flat
        %         cmocean balance
        %         axis image
        %         caxis([-5 5])
        %         colorbar
        %         title('Percentage change in area sum')
        %
        %         figure
        %         pcolor(X0,Y0,expintdiv-expintdiv2)
        %          shading flat
        %         cmocean balance
        %         axis image
        %         colorbar
        %         caxis(5e-3*[-1 1])


        3;
    end %ini day loop
    expintdivbar.(['month_',num2str(monthnum)])=expintdivTOT/length(Tvec);%average
    % expintdivbar2.(['month_',num2str(monthnum)])=expintdivTOT2/length(Tvec);%average

    disp('================================================')
    disp(['total time for full MONTH  ',...
        num2str(monthnum),' was ',num2str(toc(ticmonth)/60),' minutes'])
    disp('================================================')

    figure('numbertitle','off','name',['month_',num2str(monthnum)])
    pcolor(lonx0,latx0,expintdivbar.(['month_',num2str(monthnum)])-1)
    shading flat
    colorbar
    axis image
    cmocean bal
    caxis([-0.05 0.05])
    title(['month ',num2str(monthnum)])

end
%now save data
note={'integration done with trapz','expintdivbar is monthly means'...
    ,'expintdivMonths the data for all 12 dynamical systems in each month, reshape the first dimension into the size of data in expintdivbar' };
cd(dirr2)
save_name=[filename,'_expintdiv-climatology-',num2str(-T2)];
save(save_name,...
    'lonx0','latx0','expintdivMonths',...
    'Tvec1','T2','expintdivbar','x0','y0','note');%...
%     ,'expintdivMonths2','expintdivbar2','note')
disp('just saved')
disp(save_name)
disp('with variables')
whos('lonx0','latx0','expintdivMonths','Tvec1','T2','expintdivbar',...
    'x0','y0','note');%...
%     ,'expintdivMonths2','expintdivbar2','note')
disp('in directory')
pwd

disp('================================================')
disp(['total time for months ',num2str(monthvec),' was ',...
    num2str(toc(ticyear)/60),' minutes'])
disp('================================================')
