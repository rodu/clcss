function [C11,C22,C12,x0,y0,lon0,lat0]=...
    compute_C(lon,lat,u,v,T,tspan,save_dirr,save_note)
% [C11,C22,C12,x0,y0,lon0,lat0]=compute_C(lon,lat,u,v,T,tspan,save_dirr)
% lon = 1xNx    lat= 1xNy everything double
% tspan=1xNt
% u v should be Ny x Nx x Nt units should be km/day
% computes back in time (T is in days, should be negative) Cauchy-Green tensor
% initiatied at t0
% t0 is the time at which integration starts, of course, needs t0 \in tspan
% saves results in directory save_dirr
% use following to compute quantities of interest from output
% detC = C11.*C22 - C12.^2;
% trC = C11 + C22;
% lda2 = real(.5*trC + sqrt(.25*trC.^2 - detC));
% ftle=log(lda2)/(2*abs(T));
% see also compute_C_par which uses parallel integration
% if save_dirr and save_note are included, data is saved in directory
% save_dirr and the note describing the data will include the text in save_note
% for example save_note='velocity from altimetry';
disp([ '     ===================================================================== ';...
    '     =============== compute_C began  ',datestr(now),' =============== ';...
    '     ===================================================================== '])

% grid
lon0min = min(lon(:)); % [deg]
lon0max = max(lon(:)); % [deg]
lat0min = min(lat(:)); % [deg]
lat0max = max(lat(:)); % [deg]


lon_origin=lon0min; lat_origin=lat0min;
[x0min, y0min] = sph2xy(lon0min, lon_origin, lat0min, lat_origin);
[x0max, y0max] = sph2xy(lon0max, lon_origin, lat0max, lat_origin);
x0min = x0min*1e-3;   % [km]
y0min = y0min*1e-3;   % [km]
x0max = x0max*1e-3;   % [km]
y0max = y0max*1e-3;   % [km]

[xspan, yspan] = sph2xy(lon, lon_origin, lat, lat_origin);
xspan=xspan*1e-3; %[km]
yspan=yspan*1e-3; %[km]


% % The higher the value of N the greater the resolution, 1024 is good for
% % the Gulf of Mexico which is roughly 1200x1600 km
% N = 1024;
% % N=N/2;
% % N=N*2; % this doubles the resolution of the C tensor,
% % it will obviously be slower to compute.
% fac = (x0max-x0min)/abs(y0max-y0min);
% if fac > 1 % wider rather than tall
%    Nx0 = N;
%    Ny0 = fix(N/fac);
% else % either taller or square
%    Ny0 = N;
%    Nx0 = fix(N*fac);
% end

% Alternatively just choose 2 points in computational grid per velocity grid point
Nx0=numel(xspan)*2;
Ny0=numel(yspan)*2;
disp(['     =====     Your initial condition grid for CG is '...
    ,num2str(Ny0),' by ',num2str(Nx0),'     ======'])

Nxy0 = Nx0*Ny0;
x0 = linspace(x0min, x0max, Nx0);
y0 = linspace(y0min, y0max, Ny0);
[lon0,lat0]=xy2sph(x0*1e3, lon_origin, y0*1e3, lat_origin);
[X0, Y0] = meshgrid(x0, y0);


% computational (x,y) grid
X0 = X0(:);
Y0 = Y0(:);
dx0 = .1; %.1*mean(diff(x0));
dy0 = .1; %.1*mean(diff(y0));
xNSWE0 = [X0 X0 X0-dx0 X0+dx0]; %initial positions +- deltax
xNSWE0 = xNSWE0(:);
yNSWE0 = [Y0+dy0 Y0-dy0 Y0 Y0]; %initial positions +- deltay
yNSWE0 = yNSWE0(:);


% Prepare Cauchy-Green tensor computation
t0=tspan(end);
% time step is fixed to 0.2 days, this should be fine for most
% climatologies, but in very energetic currents consider dt=0.1 or less (make sure the CFL condition is satisfied)
% a dt that evenly divides diff(tspan) is faster and more accurate
dt=0.2; % consider tt=linspace(tspan(1),tspan(end),30*5); dt=tt(2)-tt(1); % this will create 5 points between within each day (there are 30 days)
t = t0:sign(T)*dt:tspan(1); % [t0+T t0]; length(t0+T:t0),
% the following line is for ode45 or ode23, it is commented because it is not needed if using ode4 or some other simple ode function
options=odeset('RelTol',1e-5,'AbsTol',1e-4);%

tini=t(1);
tend=t(end);
tt=t(end)-t(1);
disp(['     Integration time limits are: ',num2str([tini tend]),...
    ', total: ',num2str(sign(tt)*(abs(tt)+1)),' days']) %plus one to count zero
disp(['     Tspan (data) limits are: ',num2str([tspan(1) tspan(end)]),...
    ', total: ',num2str(tspan(end)-tspan(1)+1),' days'])

xyNSWE0=[xNSWE0;yNSWE0];
% Begin Cauchy-Green tensor computation
tic
% % can use lower order, variable step integrator if wanted
% %

[~, xyNSWE] = ode23(@uv, ...
    [tini tend], xyNSWE0, ...
    options, ...
    u, v, xspan, yspan, tspan);

% % can use lower order, fixed step integrator if wanted, might be a bit
% % faster, see other odeX integrators in repo

%     xyNSWE = ode4(@uv, ...
%        t, xyNSWE0, ...
%        u, v, xspan, yspan, tspan);
%
% toc

%   [~, xyNSWE] = ode45(@uv, ...
%             [tini tend], xyNSWE0, ...
%                 options, ...
%            u, v, xspan, yspan, tspan);
%        xyNSWE=sol.y';
%
% toc
% disp(sol.stats)

disp(['     Integration for CG tensors took ',num2str(toc/60),' min'])

xNSWE=xyNSWE(end,1:size(xyNSWE,2)/2);
yNSWE=xyNSWE(end,size(xyNSWE,2)/2+1:end);

xN = xNSWE(1:Nxy0);
xS = xNSWE(Nxy0+1:2*Nxy0);
xW = xNSWE(2*Nxy0+1:3*Nxy0);
xE = xNSWE(3*Nxy0+1:4*Nxy0);
yN = yNSWE(1:Nxy0);
yS = yNSWE(Nxy0+1:2*Nxy0);
yW = yNSWE(2*Nxy0+1:3*Nxy0);
yE = yNSWE(3*Nxy0+1:4*Nxy0);
dxdx0 = (xE - xW) / (2*dx0);
dxdy0 = (xN - xS) / (2*dy0);
dydx0 = (yE - yW) / (2*dx0);
dydy0 = (yN - yS) / (2*dy0);
dxdx0 = reshape(dxdx0, [Ny0 Nx0]);
dxdy0 = reshape(dxdy0, [Ny0 Nx0]);
dydx0 = reshape(dydx0, [Ny0 Nx0]);
dydy0 = reshape(dydy0, [Ny0 Nx0]);
C11 = dxdx0.^2 + dydx0.^2;
C12 = dxdx0.*dxdy0 + dydx0.*dydy0;
C22 = dxdy0.^2 + dydy0.^2;


if nargin>6 % will save data in save_dirr
    if nargin==8
        note=write_note(mfilename('fullpath'),save_dirr,{['period of integration (ddmm) ',...
            datestr(t0,'ddmm'),'-',datestr(t0+T,'ddmm')],save_note});
    elseif nargin==7
        note=write_note(mfilename('fullpath'),save_dirr,['period of integration (ddmm) ',...
            datestr(t0,'ddmm'),'-',datestr(t0+T,'ddmm')]);
    elseif nargin>8
        error('too many input arguments')
    end

    save([save_dirr,filesep,'traj_',datestr(t0,'ddmm'),'-', datestr(t0+T,'ddmm') ,'-xy0'],'x0','y0','note')
    save([save_dirr,filesep,'traj_',datestr(t0,'ddmm'),'-', datestr(t0+T,'ddmm') ,'-C11'],'C11','note')
    save([save_dirr,filesep,'traj_',datestr(t0,'ddmm'),'-', datestr(t0+T,'ddmm') ,'-C12'],'C12','note')
    save([save_dirr,filesep,'traj_',datestr(t0,'ddmm'),'-', datestr(t0+T,'ddmm') ,'-C22'],'C22','note')

    disp('     saved variables: x0 y0 C11 C12 C22 note')
end
disp([ '     ===================================================================== ';...
    '     ============== compute_C finished ',datestr(now),' ============== ';...
    '     ===================================================================== '])
disp(' ')
%--------------------------------------------------------------------------
function out = uv(t, xyDxy, u, v, xspan, yspan, tspan)

n = length(xyDxy)/2;
out = zeros(2*n,1);

x = xyDxy(1:n);
y = xyDxy(n+1:2*n);

x = interp1(xspan, 1:length(xspan), x); %this is original
y = interp1(yspan, 1:length(yspan), y); % this is sorting positions I think
t = interp1(tspan, 1:length(tspan), t);


t = repmat(t, [n 1]);

% % ba_interp3 is available in matlab's file exchange, and may accelerate
% % computations, but needs compiling. Try if interp3 below is too slow.
% u = ba_interp3(u, x, y, t, 'cubic');
% v = ba_interp3(v, x, y, t, 'cubic');

u = interp3(u, x, y, t, 'cubic');
v = interp3(v, x, y, t, 'cubic');

out(1:n) = u;
out(n+1:2*n) = v;


function note=write_note(mfile_name,data_directory,optional_string)
% note=write_note(mfile_name,data_directory,optional_string)
% call from program of interest with
% mfile_name=mfilename('fullpath');
% directory='C:/Something'; or if currently in data directory just call.
% The note created will be
% note=['Data created with ',mfile_name,'.m with data in ',directory,' on ',date];
% if exist('optional_string','var')
% note{1}=note;
% and
% note{kk}=optional_string{kk-1};
% end

note={['Data created with ',mfile_name,'.m']; ['saved data in  ',data_directory];['on ',datestr(now)]};


if exist('optional_string','var')

    if ~iscell(optional_string)
        temp={optional_string};
        clear optional_string;
        optional_string=temp;
    end

    note2{1}=note;
    for kk=2:length(optional_string)+1
        note2{kk}=optional_string{kk-1};%#ok
    end
    note=note2;
end


