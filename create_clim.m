function create_clim(dirr)
% create_clim cretes velocity climatology from yearly netcdf files
% modify name of u and v variables in lines  44,45
% modify climatology name in line 90, see also lines 83,84
%
pd=pwd;
ttt=tic;

disp('====================================')
disp('     ==========================     ')
disp('This code assumes all files have the same lon and lat vectors, u and v have same dimensions')
disp('This code expects one netcdf per year')
disp('This code assumes vel has 3 dimenions and time is the third dimension; if necessary, use permute.')
disp('     ==========================     ')
disp('====================================')

cd(dirr)
try
    m=string(ls('*.mat'));
catch % linux perhaps macos will error if there are no mat files
    m="";
end

if any(contains(m,'hycom_clim.mat'))
    disp('hycom_clim.mat is already in your directory, will not compute climatology again ...')
else

    d=dir('uv*.nc');% uv_yyyy.nv velocity files should be in dirr =====================================
    tot=length(d);
    disp(' ')
    disp('====================================')
    disp(['Processing ',num2str(tot),' netcdf files spanning:'])
    disp(d(1).name)
    disp(' . . .')
    disp(d(end).name)
    disp('====================================')
    disp(' ')



    ut=0;
    vt=0;

    count=0;
    c2=0;
    cnote=1;

    file=d(1).name;
    t=timevec(file);
    a=datestr(t(1));
    b=datestr(t(end));
    disp('The time vector for the first file spans')
    disp(a)
    disp('.')
    disp('.')
    disp('.')
    disp(b)
    yyyy=a(8:11); %select the year indices here and below
    disp(' ')
    disp(['I will be using ___',yyyy,'___ as the first year, if this is not correct,'])
    disp('please correct which indices are used when defining yyyy in create_clim.m')
    disp(' ')

    note1{cnote}='each year is interpolated from the data''s time spanning 01-Jan-yyyy 12:00:00 to 31-Dec-yyyy 00:00:00, unless otherwise noted, and is interpolated to time=1:360';
    for kk=1:tot
        disp('================================================================================')
        disp('================================================================================')
        count=count+1;
        file=d(kk).name;
        u=squeeze(ncread(file,'water_u'));% change if your variable names are different,
        % if you want different names per each directory add code, using "switch, case, end" where the case is based on dirr
        v=squeeze(ncread(file,'water_v'));% change if your variable names are different
        t=timevec(file);
        a=datestr(t(1));
        yyyy=a(8:11); %select the year indices here
        disp(['Processing year ',num2str(kk),', using ___',yyyy,'___ as the year'])
        %     whos u v t

        disp('= = = = = = = = = = =')
        % try
        disp(datestr(t([1 2 3])));
        disp('         .')
        disp('         .')
        disp('         .')
        disp(datestr(t([end-2 end-1 end])))
        % catch
        % disp(datestr(t([1 end])));
        % end
        disp('= = = = = = = = = = =')


        c2=c2+length(t);

        ti=gimme_datevec(['01-Jan-',yyyy,' 12:00:00'],['31-Dec-',yyyy,' 00:00:00'],360);

        if ti(end)>t(end)+2*eps(t(end)) %avoid nan when missing value at end
            cnote=cnote+1;
            ti=gimme_datevec(['01-Jan-',yyyy,' 12:00:00'],datestr(t(end)),360);
            nn=['year ',yyyy,' had to change end point of ti to ',datestr(t(end)),'; if this is a large jump, consider loading next year and interpolating to get rid of missing values at the end of current year'];
            note1{cnote}=nn;
            disp(nn)
            disp('================')
        end

        if ti(1)<t(1)-2*eps(ti(1)) %avoid nan when missing value at beggining
            cnote=cnote+1;
            ti=gimme_datevec(datestr(t(1)),['31-Dec-',yyyy,' 00:00:00'],360);
            nn=['year ',yyyy,' had to change initial point of ti to ',datestr(t(1)),'; if this is a large jump, consider loading previous year and interpolating to get rid of missing values at beginning of current year'];
            note1{cnote}=nn;
            disp(nn)
            disp('================')
        end
        ui=interp1D(t,u,ti);
        vi=interp1D(t,v,ti);

        ut=ut+ui;
        vt=vt+vi;


    end

    u=ut./count;
    v=vt./count;
    time=1:360;
    lon=double(ncread(file,'lon')); % change if your variable names are different =======================================
    lat=double(ncread(file,'lat')); % change if your variable names are different =======================================
    note{1}={['Climatology created with ',mfilename('fullpath'),'.m']; ['from data in ',dirr];['on ',datestr(now)]};
    note{2}=note1;
    files={ d(:).name}';
    note{3}=['Processed files are:'; files];

    nname='hycom_clim'; % modify climatology name here ==============================
    try
    save([dirr,filesep,nname],'u','v','time','lat','lon','note','-v7.3' )
    catch
    save([dirr,filesep,nname],'u','v','time','lat','lon','note')
    warning(['Not sure climatology was saved, -v7.3 did not work possibly due to old matlab version: search for u,v in file ',nname])
    end

    disp(' ')

    disp('===========================================')
    disp([nname,'.mat, data saved in:'])
    disp(dirr)
    whos('u','v','time','lat','lon','note')
    disp(' ')
    disp(['it took me ',num2str(toc(ttt)/60), ' minutes'])
    disp('===========================================')
    disp(' ')
    disp('Display note in file (metadata):')
    disp(note{1})
    disp(note{2})
    disp(note{3})
    disp('===========================================')
    disp(' ')
    disp(' ')
    disp(' ')
end
cd(pd)
end %function create_clim


% ==============================================================================================


function xData=gimme_datevec(Start,End,npoints)
% xData=gimme_datevec(Start,End,npoints)
startDate = datenum(Start);
endDate = datenum(End);
xData = linspace(startDate,endDate,npoints);
end


% ==============================================================================================


function xi=interp1D(t,x,ti)
% xi=interp1D(t,x,ti)
% interpolates a 3D array x along the third dimension only
% t is the coordinate for the third dimension
% assumes that in the time direction either everything is NaN (masked
% points) or has some NaN (gaps) that are interpolated
%

ti=ti(:);
t=t(:);
ii=isnan(x);
ii=all(ii,3);%find points that are NaN for all time (3rd dimension), leave other nan alone
ii=ii(:);% collapse 2D to 1D
x2=reshape(x,size(x,1)*size(x,2),size(x,3));% collapse first 2D to 1D
x2(ii,:)=-999;% get rid of nan in land points
x=reshape(x2,size(x,1),size(x,2),size(x,3)); %back to original dimensions of x
clear x2;% get rid of dummy variable


x=permute(x,[3 1 2]);% prepare for time 1D interpolation which must happen along 1st dimension

method='makima';% pchip, makima and spline will interpolate NaNs i.e. xi will not contain NaN even if x does
% spline is the more computationally expensive of the three, pchip and
% makima are C1 while spline is C2
% all three have continuous derivatives
xi=interp1(t,x,ti,method,NaN);% extrapolation set to NaN
xi=ipermute(xi,[3 1 2]);
xi(xi<-998)=NaN;% return land to NaN
end


% ==============================================================================================


function time=timevec(ncname,ind,numdays,stride)

if nargin==1

    try
        time1=ncread(ncname,'time');
        tunit=ncreadatt(ncname,'time','units');
    catch %HyCOM GoM sometimes uses MT instead of time
        try
            time1=ncread(ncname,'MT');
            tunit=ncreadatt(ncname,'MT','units');
        catch

            time1=ncread(ncname,'ocean_time');
            tunit=ncreadatt(ncname,'ocean_time','units');
        end
    end

elseif nargin>1

    try
        time1=ncread(ncname,'time',ind,numdays,stride);
        tunit=ncreadatt(ncname,'time','units');
    catch %HyCOM GoM sometimes uses MT instead of time
        time1=ncread(ncname,'MT',ind,numdays,stride);
        tunit=ncreadatt(ncname,'MT','units');
    end
end

time1=double(time1);%if time is int conversion might fail due to rounding to int

% disp('According to file attribute, units are:')
% disp(tunit)
thour=contains(tunit,{'hours','Hour','Hours','hour'});% hours
tday=contains(tunit,{'days','day','Days','Day'});% days
tsec=contains(tunit,{'seconds','Seconds','second','Second'});% seconds

% the since year should be one of the two, we can get false positives
% though for example if date is 1990-12-20 since there is a 20 and a 19 in
% the "since: date
tind20=strfind(tunit,'20');% since 2000
tind19=strfind(tunit,'19');% before 2000s (1900s)
if isempty(tind20) && ~isempty(tind19)%by checking both conditions, this is a bit more robust, note searching for 20 or 19 can lead to false positives
    tind1=tind19;
elseif ~isempty(tind20) && isempty(tind19)
    tind1=tind20;
elseif ~isempty(tind20) && ~isempty(tind19)
    error('not sure what the reference year is here, this is a case I cannot handle')
end

% disp(['Assuming this is the ''since'' year:  ',tunit(tind1:tind1+3)])

tt=tunit(tind1:end);
tt=erase(tt," UTC");
tt=replace(tt,'T',' ');
tt=erase(tt,'Z');
% disp('Procesing ''since'' date as follows:')
% disp(tt)
if thour %units hours
    %     disp('identified unit as hours, converting to days')

    if length(tt)==10%length('yyyy-mm-dd')=10
        time=(time1/24)+datenum(tt,'yyyy-mm-dd');
        %         disp('using yyyy-mm-dd format for datenum')
        %     disp(datestr(time([1 end])))
    elseif length(tt)>=19%length('yyyy-mm-dd HH:MM:SS')=19, if it has miliseconds that would be greater
        time=(time1/24)+datenum(tt,'yyyy-mm-dd HH:MM:SS');
        %         disp('using yyyy-mm-dd HH:MM:SS format for datenum')
        %     disp(datestr(time([1 end])))
    end

elseif tday  %units days
    %     disp('identified unit as days, no unit conversion, leaving as is')
    if length(tt)==10%length('yyyy-mm-dd')=10
        time=(time1)+datenum(tt,'yyyy-mm-dd');
        %         disp('using yyyy-mm-dd format for datenum')
        %     disp(datestr(time([1 end])))
    elseif length(tt)>=19%length('yyyy-mm-dd HH:MM:SS')=19
        time=(time1)+datenum(tt,'yyyy-mm-dd HH:MM:SS');
        %         disp('using yyyy-mm-dd HH:MM:SS format for datenum')
        %     disp(datestr(time([1 end])))
    end

elseif tsec  %units seconds
    %     disp('identified unit as seconds, converting to days')

    if length(tt)==10%length('yyyy-mm-dd')=10
        time=(time1/86400)+datenum(tt,'yyyy-mm-dd');
        %         disp('using yyyy-mm-dd format for datenum')
        %     disp(datestr(time([1 end])))
    elseif length(tt)>=19%length('yyyy-mm-dd HH:MM:SS')=19
        time=(time1/86400)+datenum(tt,'yyyy-mm-dd HH:MM:SS');
        %         disp('using yyyy-mm-dd HH:MM:SS format for datenum')
        %     disp(datestr(time([1 end])))
    end

end
% disp(datestr(time([1 end])))
% disp('output data spans:')
% tlims(time)
% lbn(mn)

% disp(' ... exiting timvec ...')
end
