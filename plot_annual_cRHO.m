function plot_annual_cRHO(dirr)
% plot_annual_cRHO
% plots cRHO for each month as in paper figure.
% Includes commented option for m_map library by Rich Pawlowicz for plotting
% will need to revise lines, see comments throughout 

mths={'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};
figure
monthi=1;
cx=[0.4 1.3];% A suggestion is that values of 0.4 are visualized in light green so
% values less than about 0.4 are blanked, this will show crho of about 1.5 and greater

if nargin < 1
    dirr=[pwd,filesep];
elseif nargin==1
    dirr=[dirr,filesep];% should be ok if it gets an extra filesep
end


for month=[1 4 7 10 2 5 8 11 3 6 9 12] 
    monthnum=num2str(month);
    
    dirr2=[dirr,'CG',filesep,monthnum];
%   dirr2=[dirr,'ode23',filesep,num2str(monthnum)];
disp('=============================================================')
disp('               plot_annual_cRHO using data in:               ')
disp(dirr2)
disp('=============================================================')
load([dirr2,filesep,'TOT-',monthnum],'sqrtlda2TOT','Tvec1','lonx0','latx0')
z=log(sqrtlda2TOT/length(Tvec1));% cRHO
disp(dirr2)
disp(monthnum)

subplot(3,4,monthi)
% use next line if usng m_map
% m_proj('lambert','long',[-97.6,-81.35],'lat',[18.2 30.5],'rectbox','on'); %full gulf  of Mexico, change lon lat if  you will use for different region

% use next line if usng m_map and comment pcolor below
% m_pcolor(lonx0,latx0,z)
pcolor(lonx0,latx0,z)
shading flat
colormap shadden
caxis(cx)

% hold on
% add_squeezelines(dirr,'b',1,0.45) 

% use next line if usng m_map
% m_coast('patch',0.655*[1 1 1],'edgecolor','none');

if month ==3
% use next line if usng m_map
% m_grid('tickdir','out','yaxisloc','left');
else
set(gca,'YTickLabel','','XTickLabel','')    
% use next line if usng m_map
%  m_grid('tickdir','out','yticklabels','','xticklabels','');
end

% use next line if usng m_map
% m_text(lonx0(end-30),latx0(end-30),mths{month},'fontsize',22.5,'fontweight','bold','color',0*[1 1 1])
text(lonx0(end-30),latx0(end-30),mths{month},'fontsize',22.5,'fontweight','bold','color',0*[1 1 1])

drawnow
monthi=monthi+1;
end
packboth2(3,4) %use packrows--dd=0.0105;  and packcols--dd=-0.141;
hh=colorbar('hor','Position',...
[0.385 0.035 0.43 0.02],...
    'Ticks',[0.2 0.4 0.6 0.8 1 1.2 1.4],...
    'FontWeight','bold',...
    'FontSize',16);
if min(cx) < 0.5
hh.Ticks=0.4:0.1:1.3;
else
hh.Ticks=[ 0.5 0.75 1 1.25 1.5];
hh.TickLabels={ '0.5' '0.75' '1' '1.25' '1.5'};
end
    
hh.TickDirection='in';
hh.TickLength=0.013;
hh.LineWidth=1.75;


