 **Intro**

If you intend to use this code, please notify Rodrigo Duran  rodrigo at oceanresearch.xyz and feel free to send questions or comments. 

If you use this code for your work, please cite the code and the paper where the method is developed (paper is open access):

- Duran, R., F. J. Beron-Vera and M. J. Olascoaga (2019). Climatologial Lagrangian Coherent Structures code. DOI: 10.18141/1558781 https://bitbucket.org/rodu/clcss/src/master/ 

- Duran, R., F. J. Beron-Vera, M. J. Olascoaga (2018). Extracting quasi-steady Lagrangian transport patterns from the ocean circulation: An application to the Gulf of Mexico. Scientific Reports, 8(1), 5218. https://www.nature.com/articles/s41598-018-23121-y

For the most recent updates to the code, please check commits at https://bitbucket.org/rodu/clcss/src/master/

**Summary and introductory remarks**

This code can be run in an automated way or in a more "manual" fashion. 

**The semiautomated way has two main steps**

1) Download several years of velocity data, one year per netcdf file, and save it in a dedicated directory. Each file should have the year in the name e.g. uv_1994.nc. There is some code to download velocity from HyCOM Global, see instructions below. You can get cLCS for several different velocity products, as long as you save one product (several year's worth of velocity from that velocity product) per directory.
2) Just run run_everything_cLCS.m after adding the names of your directories, the code will a) load the velocity, interpolate it to fixed times and take care of missing values, compute time-mean Cauchy-Green tensors and then compute cLCS, saving the results in the corresponding directory. Finally, a movie will be created showing how an arbitrary, conserved tracer deforms with cLCS, to aid you in the interpretation of cLCS. 
The initial interpolation part while creating the climatology may require some tweaking to be able to read the data depending on the variable (u,v,time) names. See instructions under Running everything below. 

**The manual option has four main steps**

There are four main steps to getting cLCS:
1) Create a velocity climatology (create_clim.m)
2) Load your velocity climatology and compute the time-mean Cauchy-Green tensors (meanC.m)
3) Compute cLCS from time-averaged CG tensor (compute_squeeze_lines.m)
4) Plot results (cLCScrho.m and crho.m) and visualize deformation due to cLCS (cLCS_vis.m)

Each of these steps is described below, also described is how to download velocity data and produce a climatology. Overall, obtaining cLCS should be relatively straightforward. Going from raw velocity to visualizing cLCS typically takes hours, even in large domains.  

Once you have a velocity climatology, a modern laptop can compute a 12-month climatology for the Gulf of Mexico in about 2 hours. You might want to start by replicating the results of Duran et al (2018), by downloading the velocity climatology from https://drive.google.com/open?id=0BzzNV80QAsokekNnNzBLbVM3WGs (~500 Mb) and running the code below (optional step). The figures you produce may have minor differences with the ones published, for example, due to parameters like the length of LCS and the location of initial conditions. There should, however, be a strong and obvious resemblance between the transport patterns you get and those published.

Another good first step is to read this post, which helps clarify what climatological Lagrangian Coherent Structures do, and how they can help in your research. It is not comprehensive but it should serve as a good introduction: http://oceanresearch.xyz/lagrangian-climatologies/

**Downloading velocity data from HyCOM for the velocity climatology**

First some cautions:

* Save your data in one netcdf file per year, just reading thousands of files can take hours. It is best to save one full year of data in one file. If you are using create_clim.m, then the code expects the filename to be uv_YYYY, e.g. uv_1994.nc for the 1994 velocity time series, and each file to contain one year of data (both components of the horizontal velocity, lon, lat and time).

* Try to end up with 1 velocity field per day, whether instantaneous or day averaged. Otherwise having too many fields per day over several years, could potentially complicate the process of creating a climatology. If you already have data with more than one velocity per day give it a try, it should work if your RAM can handle it. 

* Chances are, your velocity climatology does not vary much over monthly time scales. This means it should be ok if you interpolate a bit if it so happens that you have missing days (this also means one velocity per day is adequate). The interpolation in create_clim.m automatically gets rid of NaNs and gaps within the time series while respecting "land" NaNs.

* Regarding the code to download HyCOM Global (see below), the following is from HyCOM's forum: "There might be times during the day when generic DAP errors occur. If you wait 1 hour and try again and it works then it was likely rolling restart of services to incorporate new datasets."

I prefer using ncks from the nco operators to download data. To install ncks go to http://nco.sourceforge.net/ and scroll down to "Pre-built Executables", and follow instructions for your operating system. Next, run the script called download_hycom.sh (you will need the Linux subsystem for Windows if using Windows, just search for it, should be straightforward to install).  

For example, for a region with a longitude between -71 and -59 and a latitude between 8 and 19 do:
./download_hycom.sh -71.0,-59.0 8.0,19.0

(Note the .0 after integer numbers so they are recognized as floats.)

This will download data from HyCOM Global for your region, one file per year from 1994 through 2015. If some files do not download in the first attempt (internet fails etc), just run the script again with the same longitude and latitude range and in the same directory, it will download pending files. You can run the script as many times as necessary until all files have been downloaded. Note that download_hycom.sh can be easily modified to download data from other sources, provided it is available over opdendap.

You can also use ncks manually, but it is more work (not recommended). Here is an example I used recently to download data from HyCOM Global for sea surface velocity for 1994. I download one velocity every eight time records since the data is 3-hourly and I wanted one data per 24 hours (3*8=24). Just change your lon lat ranges to match your domain and do:

ncks -d time,"1994-01-01 00:00:00","1994-12-31 23:00:00",8  -d depth,0.0 -d lon,-98.0,-80.0 -d lat,24.0,30.8  -v water_v,water_u  http://tds.hycom.org/thredds/dodsC/GLBv0.08/expt_53.X/data/1994 SSV_1994.nc

For 1995 you would do

ncks -d time,"1995-01-01 00:00:00","1995-12-31 23:00:00",8  -d depth,0.0 -d lon,-98.0,-80.0 -d lat,24.0,30.8  -v water_v,water_u  http://tds.hycom.org/thredds/dodsC/GLBv0.08/expt_53.X/data/1995 SSV_1995.nc

... and so on. 

Another option (much more work) is to download the data manually (without ncks), there are instructions on how to do this in the document: "download_hycom_global_reanalysis_manually.docx", within this repository. Again, using download_hycom.sh is the easiest/fastest option. 

**Running everything automatically**    
Once you have understood the steps below, you can open run_everything_cLCS.m and enter within it the directory, or directories if you have several velocity products, where your data is. You can compute cLCS for one dataset, or many datasets by following the instructions in run_everything_cLCS.m.  Each dataset (from which velocity climatology and cLCS are computed) should have a directory of its own. Even if you use run_everything_cLCS.m to run all the code automatically, you will need to tweak a few things which are described below in the steps under "Running everything manually".

**Running everything manually**
*Creating the velocity climatology*    
(Skip this step if you already have a velocity climatology.)
 
Once you have several years of data, save the files in a directory (say dirr='C:\mydirectory'), you can now create a velocity climatology by doing create_clim(dirr). See comments in create_clim.m for some modifications that may be necessary (e.g. the name of u,v or name of lon lat depending on your data).  If you prefer to compute the climatology with your code, the process is described below (see create_clim.m for more details).  

The process to compute the climatology is as follows:
1) You may need to interpolate in time if you have time gaps in some years. Also, I interpolate the 365 or 366 days in each year to a vector spanning Jan 1 to Dec 31 of that year, but with only 360 points (days), so that each month will have 30 days and each year will have 360 days.

2) Once you have several 360-day years, average all the first days to get the first day of the climatology, average all second days to get the second day of climatology, and so on. 
For example, if your data spans the years 1994 to 2015, average:

Jan 1, 1994
Jan 1, 1995
.
.
.
Jan 1, 2015

to get the first day of climatology, and so on.

**Computing monthly- and yearly-mean Cauchy-Green Tensors** 
(Start here if you already have a velocity climatology.)

The first step is to use meanC.m to compute the monthly-averaged Cauchy-Green (CG) tensors, and a yearly-averaged CG tensor at the same time. meanC is a function that takes monthvec and dirr as arguments; monthvec can be an integer for one month (any integer from 1 to 12) or a vector of months. For example, monthvec=1:12 computes CG tensors for months 1 through 12. If you just want CG tensors for May through August, use monthvec=5:8; etc. The second argument for meanC is the directory dirr which is the directory where your climatology has been saved, see  run_everything_cLCS.m for an example of how dirr works. 

Note that when monthvec is a vector, meanC(monthvec) will compute monthly averages for each month in monthvec, and at the same time, it will compute the CG tensor averaged over the full period given by monthvec. All monthly averages, and the full yearly average, are computed only when monthvec=1:12. When monthvec is one month, the "yearly-average" will be the same as the monthly average; you can just discard the data saved for the "yearly average" in this case. To avoid confusion, the data in the "yearly average" mat file includes which months were averaged. Computing the yearly average while computing monthly averages does not add much computation time at all. If you only want the yearly-averaged CG tensor and not the monthly-averaged CG tensors, then use meanC_year instead of meanC.  meanC_year is not a function, as the argument is fixed to monthvec=1:12 and it must be run within the directory where your climatological velocity is saved.

See comments in meanC or meanC_year to adapt the code to read your velocity climatology. 

A user reported problems when running compute_C (within meanC) with Matlab R2011b, it is possible that the way interp3 was used back then has changed. I suggest using a newer version of Matlab or using ba_interp3 instead of interp3 in compute_C (or compute_C_par), search for interp3 within compute_C. Using ba_interp3 requires compiling, you can find the code and instructions in Matlab's file exchange.

meanC calls compute_C to compute each CG tensor. If you run out of memory due to a large array, or if would like to use the parallel computing toolbox, use compute_C_par instead. You will need to modify the line in meanC where compute_C is called so that it will call compute_C_par instead.  

Within compute_C or compute_C_par, the computational grid resolution is set to twice the velocity resolution. About two or more computational grid points (i.e. the grid points used to compute CG tensors) per ocean velocity grid point should work fine. However, you might want to test that your results do not change with a higher computational-grid resolution, just to make sure. (I would recommend testing a month or two as it can take a long time). 

compute_C_par uses a parfor loop to compute trajectories, so before running this code, start a parallel pool in Matlab (help parpool, or help matlabpool in older Matlab versions); note that this requires the parallel computing toolbox. Note that for small domains (order 1e3x1e3 or smaller) compute_C might be faster than compute_C_par.      

The code in compute_C_par is optimized for speed when RAM is a problem, or when you want to use a parallel for loop (parfor). In the help for compute_C_par, there are further instructions regarding this speedup, which may require some tweaking. In particular, you will need to find which value for Nseg is best. If memory continues to be a problem, increase Nseg. Increasing Nseg may increase the computation time, you will need to do a bitof testing to find an optimal Nseg value depending on the number of processors, the available RAM, and the size of your velocity grid; the code will output computation times that will help you decide. 


**Plotting c\rho**

Once the monthly-averaged CG tensors have been computed, visualize the results by plotting c\rho the climatological strength of attraction (which is very similar to a climatological FTLE). Use plot_annual_cRHO to plot cRHO for all twelve months. You can also plot crho for individual months using crho(monthnum), use crho(1:12) to plot crho for all 12 months, but each month in an individual figure.

I recommend using m_map toolbox by Rich Pawlowicz (https://www.eoas.ubc.ca/~rich/map.html) for plotting, see comments in plot_annual_cRHO. 

Use crho_yearly to plot the yearly-averaged c\rho.

**Computing cLCS**

Once you have monthly-averaged CG tensors, run compute_squeeze_lines(monthvec,dirr) where monthvec can be a number, say monthvec=5 if you want cLCS for May, or monthvec can be a vector, say monthvec=1:12 if you want cLCS for all twelve months in your year-long climatology. dirr is the same directory you used for meanC, see also run_everything_cLCS.m for an example of how dirr works. 
compute_squeeze_lines will call the code to compute squeezelines which is squeezeline.m. The last argument in squeezeline.m determines the length of your squeezeline (default ArcLength = [0 1000]). The arclength will be in the same units as x0 and y0, so currently, that is in km. Keep the first value in ArcLength equal to zero so the initial condition strategy that uses \lambda_2 maxima works well. There are two options for the initial conditions when integrating tensorlines, use the default if you do not wish to modify the code. You can select initial conditions within squeezeline (see lines 43-47, uncomment either line 48 or line 49 depending on your choice).

Use compute_squeeze_lines_yearly.m for the cLCS of the yearly-averaged CG tensor.

**Plotting and visualizing cLCS**

You can plot cLCS for each month with cLCScrho(monthvec,dirr).  To plot the yearly-averaged results use cLCScrho_yearly(dirr)) for cLCS or crho_yearly(dirr)) for the climatological strength of attraction.

plot_annual_cLCScRHO(dirr) will replicate figure 2 (plotting all months) of the paper freely available at https://www.nature.com/articles/s41598-018-23121-y but using your own data. You might need to edit the value of dd in packcols2 and packrows2 to get the subplots close enough to each other while not overlapping. 

You can also add squeezelines to any plot you create by using add_colored_squeezelines.m and add_squeezelines.m. The former plots squeezelines with color that indicates the value of c\rho, and the latter plots black squeezelines. See the code in cLCScrho.m for an example of how to use add_colored_squeezelines. Note that both add_colored_squeezelines.m and add_squeezelines.m assume you are using the m_map toolbox. 

If you are not using m_map to plot, and would like to plot cLCS in lon lat directly, use add_squeezelines_lonlat.m and add_colored_squeezelines_lonlat.m

If you have a large domain or many squeezelines, the routines that interpolate (i.e. when using colored squeezeline) can be a bit slow, shouldn't be too bad though. Didn't spend much time thinking how to speed this up, but if you have suggestions please email me.

Interpreting cLCS is not always straightforward, cLCS_vis helps illustrate what cLCS mean, it produces a movie that animates recurrent and persistent Lagrangian transport patterns and how cLCS delineate them. 


**The role of horizontal divergence**

The role of the velocity divergence can be quantified, thus gaining an understanding on the effect that the vertical velocity has on cLCS, even though we only use a two-dimensional velocity. To this purpose, included is the code needed to replicate Appendix A in the Supplemental Information of Duran et al. (2018). The direct link to the Supplemental Information is https://static-content.springer.com/esm/art%3A10.1038%2Fs41598-018-23121-y/MediaObjects/41598_2018_23121_MOESM1_ESM.pdf . Please start by reading Appendix A "The role of divergence"

To compute \alpha (the Jacobian determinant) use F_div.m You will need to modify line 24 to load your climatological velocity, also make sure the grid (line 41--86) and the value of T is the same as in compute_C, this is so the dynamical systems are the same. 

plot_lagrangian_div_histogram.m will plot the change of area due to lagrangian divergence as a function of trajectory initial positions, and the histograms for change of area due to Lagrangian divergence, similar to figures 2 and 3 of Appendix A.

To compute the Eulerian divergence and then plot the monthly-mean divergence use compute_divergence.m and then run plot_monthly_mean_div.m

For the plotting, you might want to use colormaps found in https://www.mathworks.com/matlabcentral/fileexchange/57773-cmocean-perceptually-uniform-colormaps


**Acknowledgments**

Work by R. Duran was in support of the U.S. Department of Energy, National Energy Technology Laboratory's ongoing Offshore Research program under RES contracts DE-FE0004000 and DE-FE1022409 

The work of F.J. Beron-Vera and M.J. Olascoaga was supported by the Gulf of Mexico Research Initiative as part of the Consortium for Advanced Research on Transport of Hydrocarbon in the Environment (CARTHE) and SENER/CONACyT grant 201441 as part of the Consorcio de Investigacion del Golfo de Mexico (CIGoM).

See also acknowledgments in https://www.nature.com/articles/s41598-018-23121-y 

This repository includes code by RD, FJBV, and JMO. squeezeline.m was adapted by FJBV from code in https://github.com/LCSETH/Elliptic_LCS_2D and added upon by RD. Please see their license at https://github.com/LCSETH/Elliptic_LCS_2D/blob/master/license.txt

Patrick Wingo (NETL-DOE) kindly provided download_hycom.sh 

Several programs by Jonathan Lilly (jLab) are used for plotting, see http://www.jmlilly.net/jmlsoft.html  

For those using m_map, please visit https://www.eoas.ubc.ca/~rich/map.html
 
Several researchers have helped test and improve the code. Special thanks to Dr. Mainara Gouveia, National Institute for Space Research Brazil (INPE), Ana Ramirez-Manguilar, Universidad Nacional Autonoma de Mexico (UNAM), Dr. Justino Martinez, Institut de Ciències del Mar in Barcelona and Dr. Gaby Mayorga-Adame, National Oceanography Center in UK.

**Copyright and Disclaimer**

Copyright (c) 2019, 2020, 2021, 2022 Rodrigo Duran, Francisco Javier Beron-Vera, and Josefina Olascoaga, except copyright held by others, as noted in Acknowledgments.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

This project was funded by the Department of Energy, National Energy Technology Laboratory, an agency of the United States Government, through a support contract with AECOM. Neither the United States Government nor any agency thereof, nor any of their
employees, nor AECOM, nor any of their employees, makes any warranty, expressed or implied, or assumes any legal liability or responsibility for the accuracy, completeness, or usefulness of any information, apparatus, product, or process disclosed, or represents that its use would not infringe privately owned rights. Reference herein to any specific commercial product, process, or service by trade name, trademark, manufacturer, or otherwise, does not necessarily constitute or imply its endorsement, recommendation, or favoring by the United States Government or any agency thereof. The views and opinions of authors expressed herein do not necessarily state or reflect those of the United States Government or any agency thereof.

**Regarding computation times and resolution for LCS computation**

Summary: A typical regional domain will need about an hour to compute all twelve CG tensors, and about another 10 minutes to compute all twelve monthly cLCS from there. More details and several examples are given below. Even with a large domain of 1000x1000x360, computing cLCS from a climatology and visualizing them takes hours in a modern computer. 


With 4 processors running at about 3.1-3.3 GHz and 16 Gb of RAM, using interp3 (not ba_interp3) within compute_C_par,  and N=1024 (which results in a computational grid of 798x1024) within compute_C_par, I can compute the monthly-mean CG tensors for all 12 months of the GoM climatology in about 90 minutes. With twice the resolution (N=2*N; in compute_C_par, i.e. using a grid of 2048x1596 in the Gulf of Mexico) computing the 12 CG tensors for one month took about 33 min. on four processors, so it would take about 6 hours for all 12 months. So doubling the resolution quadrupled the computation time. In a different computer and without using parallel code, a computational grid of 750 by 594 took only 3.3 minutes per month.

With a different processor, the same RAM, again using interp3 and N=1024 one-month using compute_C takes about 10 min. and using compute_C_par with 4 processors and Nseg=4 one month takes about 7 minutes, not a huge speed up. Still, for large domains, compute_C_par may be necessary.

N=1024 worked fine for the Gulf of Mexico which is roughly 1200x1600 km and has a model resolution of 4km, there was no improvement when the resolution was doubled (see appendix of paper). 

For a region of 4000x6000 km and an ocean-model resolution of 9km, N=2*N; in compute_C worked fine. The velocity was 954x526 points (longitude x latitude) and N=2*N resulted in a computational grid of 2048x1136, using 12 processors and Nseg=32 within compute_c_par and Matlab 2013a, each CG tensor took about 3 minutes, a full month (12 CG tensors) took 36 minutes. The same computation on Matlab 2018a took about the same time so it does not seem that JIT compilation (introduced in 2015b) makes a big difference, at least not over a single month (subsequent months may be faster as the code is reused).

Computing cLCSs from one time-averaged CG tensor with compute_squeeze_lines is usually fast (less than a minute). Several tests suggest it may take up to about 20 minutes when your domain is large, and you request many squeezelines and long squeezelines. 

So overall a modern laptop can compute a twelve-month climatology for a region the size of the Gulf of Mexico in less than two hours.
