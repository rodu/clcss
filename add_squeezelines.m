function add_squeezelines(dirr,monthnum,varargin)
% add_squeezelines(dirr,monthnum,linewidth)
% loads LCS in file filename within directory dirr 
% adds squeezelines to current figure plotting them in black
% lon0 and lat0 is just some longitude and latitude value (one number) in your domain
% it is used to transform x y to lon lat. For example, use lon0=min(lonx0(:)); lat0=min(latx0(:)); 
% see also add_colored_squeezelines.m add_squeezelines_lonlat add_colored_squeezelines_lonlat
disp('   Begin adding squeezelines ...      =====================')
ttic=tic;

switch nargin
    case 2
        lw=1.25;%linewidth     
    case 3
        lw=varargin{1};
end
mstr=num2str(monthnum);
dirr2=[dirr,filesep,'CG',filesep,mstr,filesep];
filename=[dirr2,'cLCS_',mstr];
filename2=[dirr2,'TOT-',mstr];

try 
aa=load(filename);
catch
   error(['Could not find mat file ',filename,' in ',dirr])
end

try 
bb=load(filename2,'latx0','lonx0');
catch
   error(['Could not find mat file ',filename2,' in ',dirr])
end
lon0=min(bb.lonx0(:));
lat0=min(bb.latx0(:));


nLCS = size(aa.pxt,2);
hold on
ind=3; % use ind>1 if you want to skip plotting some cLCS
% ind = 2 or 3 is good for when squeezelines are computed froma a regular grid of ICs
for kk = 1:ind:nLCS
%     xs=xx(:,kk);
%     ys=yy(:,kk);
    xs=aa.pxt(:,kk);
    ys=aa.pyt(:,kk);
    
    [xs, ys]=xy2sph(xs*1e3, lon0, ys*1e3, lat0);
    [xs,ys]=m_ll2xy(xs,ys);%requires m_map
    plot(xs, ys,'k','LineWidth',lw)
%     h=plot(xs, ys,'k','LineWidth',lw);
%     uistack(h,'bottom');
end

drawnow
disp('   Done  adding squeezelines ...      =====================')
disp(['   adding squeezelines took ',num2str(toc(ttic)/60),' min'])
% cd(dirr2)
