function meanC_general(dirr)
% meanC_general(dirr)
% meanC_general computes sequential Cauchy-Green tensors, and the average for
% the full period
% Revise line 33 to read your velocity; units should be m/s, it will
% be converted to km/day.
% Check velocity-array dimensions in lines 52--56, should be Ny x Nx x Nt
% to compute cLCS see compute_squeeze_lines.
% see also: meanC

lb2
disp(['starting meanC_general ' datestr(now)])
lb2
ticyear=tic;

T=-7;% 7-day CG tensors
NumSteps=2;% initiate a new CG tensore every NumSteps, default is one every other day (NumSteps=2)

% choose directory where you would like to save your results, for now it
% uses pwd as default
if nargin < 1
    dirr=[pwd,filesep];
elseif nargin==1
    dirr=[dirr,filesep];% should be ok if it gets an extra filesep
end
% results will be saved in subdirectory:
dirr2=[dirr,'All_CG',filesep];
mkdir(dirr2)


% velocity should me in meters / second
% time dimension should be third dimension
load([dirr,'vel_file'],'u','v','time','lon','lat') %load velocity with variables: lon lat u v time
assert(isvector(lon) & isvector(lat),'lon and lat should be vectors, not arrays')
disp('========================================================================')
disp(' ')
warning('=========== meanC_general assumes velocity is in meters/second ============')
warning('=========== meanC_general assumes time is in days              ============')
disp(' ')
disp('========================================================================')

if iscolumn(lon) %lon lat must be row vectors
    lon=lon.';
end
if iscolumn(lat)
    lat=lat.';
end

lon=double(lon);lat=double(lat); %hycom writes lon lat in single precision, need double.


if size(u,1)==length(lon)
    u=permute(u,[2 1 3]); %vel should have dimensions Ny x Nx x Nt
    v=permute(v,[2 1 3]); %vel should have dimensions Ny x Nx x Nt
    warning('=========== permuted velocity, it should have dimensions Ny x Nx x Nt  ============')
end

%prepare boundary conditions
numBC=2;
u(1:numBC,:,:)=0;v(1:numBC,:,:)=0; %south
u(end-numBC:end,:,:)=0;v(end-numBC:end,:,:)=0;%north

u(:,1:numBC,:)=0;v(:,1:numBC,:)=0; %west
u(:,end-numBC:end,:)=0;v(:,end-numBC:end,:)=0;%east

u=u*86.4; % m/s --> km/day
v=v*86.4;

inan=isnan(u+v);
u(inan)=0;
v(inan)=0;

Nt=size(u,3);

if ~exist('time','var')
    dtt=1; % appropriate for one value daily, use dt=1/2 for 2 daily values
    time=1:dtt:Nt;
end
assert(Nt==length(time),'Third velocity dimension should be time, something is wrong here')
dt=diff(time);% units should be day
assert(max(abs(diff(dt)))<1e-6,'Error dt is not uniform, please use constant dt')% if time is single precision and dt is uniform, the difference between dts should not be greater than 1e-6, i think --- might need to tweek this
dataperday=1/dt(1);
assert(round(dataperday)==dataperday,'dataperday should be an integer')
ddt=dt(1);

% prepare for loop
clear note
note{1}=['created with ',mfilename,' with data in ',dirr];

count=0;
lda2total=0;
sqrtlda2total=0;
ftletotal=0;
C11total=0;
C22total=0;
C12total=0;

Tvec=(T:-NumSteps:-Nt)-1;

for Tind=1:length(Tvec)
    count=count+1;
    T2=Tvec(Tind);
    iniday=abs(T2);
    %     numdays=abs(T2)*dataperday;
    timeindex=((iniday+T2+ddt)*dataperday:dataperday:(iniday)*dataperday);
    tspan=time(timeindex);
    disp('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
    disp(['= =           New iniday ',pad(num2str(iniday),3),'                        = ='])
    disp('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')

    disp(datestr(tspan))
    um=u(:,:,timeindex);
    vm=v(:,:,timeindex);

    [C11,C22,C12,x0,y0,lonx0,latx0]=compute_C(lon,lat,um,vm,T,tspan,dirr2);

    % % FTLE, rho, C, lambda2 and their averaging. TOT will be for total sum
    detC = C11.*C22 - C12.^2;
    trC = C11 + C22;
    lda2 = real(.5*trC + sqrt(.25*trC.^2 - detC));
    ftle=log(lda2)/(2*abs(T));
fname=[dirr2,filesep,'CG_T=',num2str(T),'_',num2str(count)];
save(fname,'lonx0','latx0','lda2','T','ftle','C11',...
    'C22','C12','x0','y0','note','count','timeindex')
disp([newline,'just saved ',fname,newline])
disp('with variables')
whos('lonx0','latx0','lda2','T','ftle','C11',...
    'C22','C12','x0','y0','note','count','timeindex')

    lda2total=lda2+lda2total;
    sqrtlda2total=sqrtlda2total+sqrt(lda2);
    ftletotal=ftletotal+ftle;
    C11total=C11total+C11;
    C22total=C22total+C22;
    C12total=C12total+C12;


end %ini day loop


lb2
disp(['total time was ',num2str(toc(ticyear)/60),' minutes'])
lb2


note{2}='to get the time-averaged CG (over the full period) do e.g. meanC11=C11total/count;';


save([dirr2,filesep,'time-averaged-CG']...
    ,'lonx0','latx0','lda2total','sqrtlda2total','T','ftletotal','C11total',...
    'C22total','C12total','x0','y0','note','count')
disp([newline,'just saved'])
disp([dirr2,filesep,'time-averaged-CG',newline])
disp('with variables')
whos('lonx0','latx0','lda2total','sqrtlda2total','T','ftletotal','C11total',...
    'C22total','C12total','x0','y0','note','count')

figure
pcolor(lonx0, latx0, log(sqrtlda2total/count))
shading flat
colormap shadden
% caxis([0.3 1.3])
colorbar
axis image
drawnow
title('time averaged strength of attraction')

lb2
disp(['finished meanC_general ' datestr(now)])
lb2

