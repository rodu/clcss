function h=add_squeezelines_lonlat...
    (dirr,monthnum,varargin)
% h=add_squeezelines(dirr,filename,linewidth)
% loads cLCS within directory [dirr,filesep/CG/num2str(monthnum)]
% and adds squeezelines to current figure, which is a plot of a function of
% longitude and latitude for your cLCS domain, % plotting them in black
% Default linewidth is 1.25
% if requested, returns h which is a structure with all squeezelines
% see also: add_colored_squeezelines add_squeezelines add_colored_squeezelines_lonlat
disp('   Begin adding squeezelines ...      =====================')
ttic=tic;

switch nargin
    case 2
        lw=1.1;%linewidth     
    case 3
        lw=varargin{1};
end

mstr=num2str(monthnum);
dirr2=[dirr,filesep,'CG',filesep,mstr,filesep];
filename=[dirr2,'cLCS_',mstr];
filename2=[dirr2,'TOT-',mstr];

try 
aa=load(filename);
catch
   error(['Could not find mat file ',filename,' in ',dirr])
end

try 
bb=load(filename2,'latx0','lonx0');
catch
   error(['Could not find mat file ',filename2,' in ',dirr])
end
lon0=min(bb.lonx0(:));
lat0=min(bb.latx0(:));

%prepare to plot and plot
nLCS = size(aa.pxt,2);
hold on
ind=2; % use ind>1 if you want to skip plotting some cLCS
% ind = 2 or 3 is good for when squeezelines are computed froma a regular grid of ICs
if nargout==1
for kk = 1:ind:nLCS
%     xs=xx(:,kk);
%     ys=yy(:,kk);
    
    xs=aa.pxt(:,kk);
    ys=aa.pyt(:,kk);

    [xs, ys]=xy2sph(xs*1e3, lon0, ys*1e3, lat0);
    h.(['c',num2str(kk)])=plot(xs, ys,'k','LineWidth',lw);
%     h=plot(xs, ys,'k','LineWidth',lw);
%     uistack(h,'bottom');
end
elseif nargout==0
for kk = 1:ind:nLCS
%     xs=xx(:,kk);
%     ys=yy(:,kk);
    
    xs=aa.pxt(:,kk);
    ys=aa.pyt(:,kk);

    [xs, ys]=xy2sph(xs*1e3, lon0, ys*1e3, lat0);
    plot(xs, ys,'k','LineWidth',lw)
%     h=plot(xs, ys,'k','LineWidth',lw);
%     uistack(h,'bottom');
end

end



drawnow
disp('   Done  adding squeezelines ...      =====================')
disp(['   adding squeezelines took ',num2str(toc(ttic)/60),' min'])
