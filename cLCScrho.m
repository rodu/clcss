function hc=cLCScrho(monthvec,dirr)
% hc=cLCScrho(monthvec,dirr)
% this plots cLCS colores with cRHO
% change directory so dirr matches where you monthly directories are (line 15)
% monthvec is a vector of all months for which you want to plot cLCS
% colored by crho. hc includes all the cLCS
%
% to move cLCS upwards in the plot for visibility, call cLCScrho with output:
% hc=cLCScrho(monthvec)
% and then do
% nn=fieldnames(hc);
% for kk=1:length(nn), uistack(hc.(nn{kk}),'top'), end
%
% Example:
% cLCScrho(1:12) % plots all 12 months
% see also crho
cx=[0.3 1.3];% A suggestion is that values of 0.4 are visualized in light green so
% values less than about 0.4 are blanked, this will show crho of about 1.5 and greater

lw=1.3; %linewidth for cLCS, might want to adjust

% choose directory from which you would like to read your results, 
% for now it uses pwd as default
if nargin < 2
    dirr=[pwd,filesep];
elseif nargin==2
    dirr=[dirr,filesep];% should be ok if it gets an extra filesep
end

for monthnum=monthvec
    dirr2=[dirr,filesep,'CG',filesep,num2str(monthnum)];
    disp('=============================================================')
    disp('cLCScrho is using data in:                   ')
    disp(dirr2)
    disp('=============================================================')

    load([dirr2,filesep,'TOT-',num2str(monthnum)])%#ok
    lonmin=min(lonx0(:));%#ok
    latmin=min(latx0(:));%#ok
    lonmax=max(lonx0(:));
    latmax=max(latx0(:));
    z=log(sqrtlda2TOT/length(Tvec1));

    name=['cLCS c\rho month ',num2str(monthnum)];
    figure('numbertitle','off','name',name)
    clf
    % % use following lines if not using m_map ================================
    % % colored squeezelines
    % add_colored_squeezelines_lonlat(dirr2,['cLCS_',num2str(monthnum)]...
    %     ,z,x0,y0,lonmin,latmin,lw)
    % %========================================================================


    % % use following lines if not using m_map ================================
    % % black squeezelines
    % add_squeezelines_lonlat(dirr2,['cLCS_',num2str(monthnum)]...
    %     ,lonmin,latmin,lw)
    % %========================================================================


    % % use following lines if  using m_map ====================================
    m_proj('lambert','long',[lonmin,lonmax],'lat',[latmin latmax]... % choose your projection here
        ,'rectbox','on');
    hc=add_colored_squeezelines(dirr2,['cLCS_',num2str(monthnum)]...
        ,z,x0,y0,lonmin,latmin,lw); %,x0,y0,lon0,lat0,varargin
    % m_usercoast('GoM_coast_i','patch',0.55*[1 1 1],'edgecolor','black');
    m_coast('patch',0.55*[1 1 1],'edgecolor','black');
    % m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
    m_grid('tickdir','out','yaxisloc','left');
    colorbar
    colormap shadden
    caxis(cx)
    % % ========================================================================

    disp('=============================================================')

end
