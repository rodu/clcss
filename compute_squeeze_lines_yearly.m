function compute_squeeze_lines_yearly(dirr)
% compute_squeeze_lines_yearly(dirr)
% Load yearly-averaged C-G, and compute squeezelines
% see also cLCScrho_yearly for plotting
disp(datestr(now))

% choose directory where you would like to save your results,
% for now it uses pwd  as default
if nargin < 1
    dirr=[pwd,filesep];
elseif nargin==1
    dirr=[dirr,filesep];% should be ok if it gets an extra filesep
end


disp('===========================================================================')
disp('                         Just began yearly cLCS ')
disp('===========================================================================')

ticmonth=tic;
dirr2=[dirr,'CG',filesep,'fullyear',filesep];
disp('compute_squeeze_lines_yearly will be using data in:            ')
disp(dirr2)
disp('===========================================================================')



load([dirr2,'yearly-averaged-CG']...
    ,'C11year','C12year','C22year','x0','y0','count_year')

N=count_year;
% N=length(Tvec1);
% assert(N==count_year,'There is a problem, make sure meanC_year was run correctly')
C11=C11year/N;
C12=C12year/N;
C22=C22year/N;
[pxt,pyt] = squeezeline(C11,C12,C22,x0,y0,[0 1000]);

save([dirr2,'cLCS_yearly'],'pxt','pyt')
disp([newline,'Just saved cLCS in',newline,pwd,newline])
disp(['Search for ','cLCS_yearly.mat with variables :'])
whos('pxt','pyt')

disp(['Finished, total time for yearly was ',num2str(toc(ticmonth)/60),' min.'])
disp('===========================================================================')
