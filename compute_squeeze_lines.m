function compute_squeeze_lines(monthvec,dirr)
% compute_squeeze_lines(monthvec)
% Load averaged C-G, and compute squeezelines
% see also add_colored_squeezelines for plotting

disp(datestr(now))
% T2vec=-7;
% choose directory where you would like to save your results, for now it
% uses pwd as default
if nargin < 2
    dirr=[pwd,filesep];
elseif nargin==2
    dirr=[dirr,filesep];% should be ok if it gets an extra filesep
end

for monthnum=monthvec%1:12;
disp(' ')
    disp('===========================================================================')
    disp(['     compute_squeeze_lines just began a new month ',num2str(monthnum) ])
    disp('===========================================================================')
    ticmonth=tic;
    %choose one of the two following options:
    dirr2=[dirr,'CG',filesep,num2str(monthnum),filesep];

    disp('compute_squeeze_lines will be using data in:            ')
    disp(dirr2)

    load([dirr2,'TOT-',...
        num2str(monthnum)],...
        'C11TOT','C12TOT','C22TOT','x0','y0','Tvec1')

    N=length(Tvec1);
    C11=C11TOT/N;
    C22=C22TOT/N;
    C12=C12TOT/N;
    ArcLength=1000;
    disp([newline,'The length of squeezelines is ',num2str(ArcLength)])
    disp('===========================================================================')
    [pxt,pyt] = squeezeline(C11,C12,C22,x0,y0,[0 ArcLength]);
    save([dirr2,'cLCS_',num2str(monthnum)],'pxt','pyt')
    disp(['===========================================================================',newline])
    disp(['Just saved cLCS in',newline,pwd,newline])
    disp(['Search for ','cLCS_',num2str(monthnum),'.mat with variables :'])
    whos('pxt','pyt')


    disp([newline,'Total time for this month ',num2str(toc(ticmonth)/60),' min.',newline])
    disp('===========================================================================')
    disp(' ')
end %month loop


