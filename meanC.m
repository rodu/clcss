function meanC(monthvec,dirr)
% meanC(monthvec,dirr)
% meanC computes monthly-averaged Cauchy-Green tensors, and the average for
% full period according to months in monthvec.
% For all twelve months use meanC(1:12) this produces monthly-averages
% and a yearly average at the same time, without much overhead.
% Revise line 28 to read your velocity; units should be m/s, it will
% be converted to km/day.
% Check velocity-array dimensions in lines 46--50, should be Ny x Nx x Nt
% to compute cLCS see compute_squeeze_lines.
disp('========================================================================')
disp(['starting meanC     ' datestr(now)])
disp('========================================================================')

if nargin == 1 && ischar(monthvec)
    dirr=[monthvec,filesep];
    monthvec=1:12;
    warning(['Monthvec not properly defined; assuming you want all 12 months, assuming your directory is ',dirr])
elseif nargin == 1 && isnumeric(monthvec)
    % choose directory where you would like to save your results, for now it uses pwd as default
    dirr=[pwd,filesep];
    disp(['Assuming your directory is ',dirr])
elseif nargin==2
    dirr=[dirr,filesep];% should be ok if it gets an extra filesep
end


ticyear=tic;

T2=-7;% -7 is the value for the T described in paper, see meanC_general if you would like to test with different values




% velocity should me in meters / second
% time dimension should be third dimension
load([dirr,'hycom_clim'],'u','v','time','lon','lat') %load climatological velocity with variables: lon lat u v time
assert(isvector(lon) & isvector(lat),'lon and lat should be vectors, not arrays')
disp(' ')
disp('=========== meanC assumes velocity is in meters/second ============')
disp(' ')
disp('========================================================================')

if iscolumn(lon) %lon lat must be row vectors
    lon=lon.';
end
if iscolumn(lat)
    lat=lat.';
end

lon=double(lon);lat=double(lat); %hycom writes lon lat in single precision, need double.


if size(u,1)==length(lon)
    if size(u,1)==size(u,2)
        warning("First and second dimenions are the same, can't distinguish whether permute is necessary or not")
    end
    whos u v lon lat time
    u=permute(u,[2 1 3]); %vel should have dimensions Ny x Nx x Nt
    v=permute(v,[2 1 3]); %vel should have dimensions Ny x Nx x Nt
    warning('=========== permuted velocity, it should have dimensions Ny x Nx x Nt  ============')
    whos u v lon lat time
end


%prepare boundary conditions
numBC=3;
u(1:numBC,:,:)=0;v(1:numBC,:,:)=0; %south
u(end-numBC+1:end,:,:)=0;v(end-numBC+1:end,:,:)=0;%north

u(:,1:numBC,:)=0;v(:,1:numBC,:)=0; %west
u(:,end-numBC+1:end,:)=0;v(:,end-numBC+1:end,:)=0;%east

u=u*86.4; % m/s --> km/day
v=v*86.4;

inan=isnan(u+v);
u(inan)=0;
v(inan)=0;

if ~exist('time','var')
    dtt=1; % appropriate for one value daily, use dt=1/2 for 2 daily values
    time=1:dtt:size(u,3);
end

dt=diff(time);% units should be day
assert(max(abs(diff(dt)))<1e-6,'Error dt is not uniform, please use constant dt, see create_clim.m')% if time is single precision and dt is uniform, the difference between dts should not be greater than 1e-6, i think --- might need to tweek this
dataperday=1/dt(1);
assert(round(dataperday)==dataperday,'dataperday should be an integer')
ddt=dt(1);
count_year=0;
lda2year=0;
sqrtlda2year=0;
ftleyear=0;
C11year=0;
C22year=0;
C12year=0;
for monthnum=monthvec%1:12;
    ticmonth=tic;
    dirr2=[dirr,'CG',filesep,num2str(monthnum)];
    mkdir(dirr2)

    Tvec1=(T2:-2:-30)-1;
    Tvec=Tvec1-((monthnum-1)*30);%same length as Tvec1, note that each month has 30 days.
    lda2TOT=0;
    sqrtlda2TOT=0;
    ftleTOT=0;
    C11TOT=0;
    C22TOT=0;
    C12TOT=0;
    for Tind=1:length(Tvec)
        T=Tvec(Tind);
        iniday=abs(T);
        %     numdays=abs(T2)*dataperday;
        timeindex=((iniday+T2+ddt)*dataperday:dataperday:(iniday)*dataperday);
        tspan=time(timeindex);
        disp('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')
        disp(['= =           New iniday ',pad(num2str(iniday),3),' --- month ',pad(num2str(monthnum),2),...
            '         = ='])
        disp('= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =')

        disp(datestr(tspan))
        umonth=u(:,:,timeindex);
        vmonth=v(:,:,timeindex);


        [C11,C22,C12,x0,y0,lonx0,latx0]=compute_C(lon,lat,umonth,vmonth,T2,tspan,dirr2);
        %     [C11,C22,C12,x0,y0,lonx0,latx0]=compute_C_par(lon,lat,umonth,vmonth,T2,tspan,dirr2);


        % % FTLE, rho, C, lambda2 and their averaging. TOT will be for monthly total sum
        detC = C11.*C22 - C12.^2;
        trC = C11 + C22;
        lda2 = real(.5*trC + sqrt(.25*trC.^2 - detC));
        ftle=log(lda2)/(2*abs(T2));

        lda2year=lda2+lda2year;
        sqrtlda2year=sqrtlda2year+sqrt(lda2);
        ftleyear=ftleyear+ftle;
        C11year=C11year+C11;
        C22year=C22year+C22;
        C12year=C12year+C12;
        count_year=count_year+1; % count another CG tensor

        C11TOT=C11+C11TOT;
        C22TOT=C22+C22TOT;
        C12TOT=C12+C12TOT;
        lda2TOT=lda2TOT+lda2;
        sqrtlda2TOT=sqrtlda2TOT+sqrt(lda2);
        ftleTOT=ftle+ftleTOT;


    end %ini day loop

    lb2
    disp(' ')
    disp(' ')
    disp(['Finished averaging CG, total time for full MONTH ',num2str(monthnum),' was ',num2str(toc(ticmonth)/60),' minutes'])
    disp(' ')
    disp(' ')
    lb2

    note='to get monthly average do e.g. lda2TOT/length(Tvec1)';
    save([dirr2,filesep,'TOT-',num2str(monthnum)]...
        ,'lonx0','latx0','lda2TOT','sqrtlda2TOT','Tvec1','T2','ftleTOT','C11TOT','C22TOT','C12TOT','x0','y0','note')
    disp([newline,'just saved'])%sqrtlda2TOT
    disp([dirr2,filesep,'TOT-',num2str(monthnum),newline])
    disp('with variables')
    whos('lonx0','latx0','lda2TOT','sqrtlda2TOT','Tvec1','T2','ftleTOT','C11TOT','C22TOT','C12TOT','x0','y0','note')


    % comment if you don't want to plot
    figure
    % [lonx0, latx0]=xy2sph(x0*1e3, min(lon), y0*1e3, min(lat));
    pcolor(lonx0, latx0, log(sqrtlda2TOT/length(Tvec1)))
    shading flat
    colormap shadden
    colorbar
    caxis([0 1.5])
    title(['monthly average strength of attraction c\rho ',num2str(monthnum)])
    axis image
    drawnow

end %month loop
lb2
disp(['total time for month(s) ',num2str(monthvec),' was ',num2str(toc(ticyear)/60),' minutes'])
lb2

clear note
note{1}='to get average do e.g. sqrtlda2year/count_year';
if numel(monthvec)~=12
    warning('loop did not go through all 12 months, that is ok just check monthvec to see which months were averaged and saved under yearly-averaged-CG')
    note{2}='this is not a yearly average, see monthvec vector to check which month(s) where averaged';
end

dirr2=[dirr,'CG',filesep,'fullyear'];
mkdir(dirr2)
save([dirr2,filesep,'yearly-averaged-CG']...
    ,'lonx0','latx0','lda2year','sqrtlda2year','T2','ftleyear','C11year',...
    'C22year','C12year','x0','y0','note','monthvec','count_year')
disp([newline,'just saved'])
disp([dirr2,filesep,'yearly-averaged-CG',newline])
disp('with variables')
whos('lonx0','latx0','lda2year','sqrtlda2year','T2','ftleyear','C11year',...
    'C22year','C12year','x0','y0','note','monthvec','count_year')

figure
pcolor(lonx0, latx0, log(sqrtlda2year/count_year)) % natural log of sqrt(lda2) yearly mean
shading flat
colormap shadden
caxis([0.3 1.5])
colorbar
title(['sqrtlda2 averaged over month(s) ',num2str(monthvec)])
axis image
drawnow

lb2
disp(['finished meanC ' datestr(now)])
lb2

