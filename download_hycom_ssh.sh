#! /bin/bash

# ./download_hycom_ssh.sh -97.84,-81.12 18.0,30.96
# cd /mnt/f/data/OceanVelocity/HyCOM_Global_GoM_1994_2015_reanalysis_GOFS3p1

lonRange=$1
latRange=$2

c1=0
c2=0

for year in {1994..2015}
do
    outFile=ssh_$year.nc
    
    if [ -f "$outFile" ]; then
        echo "$outFile exists"
		((c1=c1+1))
    else
		((c2=c2+1))
        echo "Retrieving $year =================="
        echo "lon $lonRange"
        echo "lat $latRange"
        ncks -d time,"$year-01-01 00:00:00","$year-12-31 23:00:00",8 -d lon,$lonRange -d lat,$latRange -d depth,0.0 -v surf_el http://tds.hycom.org/thredds/dodsC/GLBv0.08/expt_53.X/data/$year $outFile
    fi
	
done
echo $c1 files have been found locally
echo $c2 files have been downloaded




