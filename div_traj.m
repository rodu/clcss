function [x,y,t]=div_traj(u,v,xspan,yspan,tspan,X0,Y0,t)
% [x,y,t]=div_traj(u,v,xspan,yspan,tspan,X0,Y0,t)
% (u,v) is the velocity as a function of (xspan,yspan,tspan); 
% (X0,Y0) is a meshgrid of initial conditions
% and t is the time at which the solution is
% outputted for example when using ode45 the 
% solution is evaluated at t: deval(sol2,t)
% returns trajectories (x(t),y(t),t) by integrating 
% (u(xspan,yspan,tspan),v(xspan,yspan,tspan))
% with initial conditions (X0,Y0). 
% 
% lon = 1xNx    lat= 1xNy     u v in km/day  everything double
% example below for how to compute (xpsan,yspan) from (lon,lat):
% [xspan, yspan] = sph2xy(lon, lon(1), lat, lat(1));
% xspan=xspan*1e-3; %[km]
% yspan=yspan*1e-3; %[km]

X0 = X0(:);
Y0 = Y0(:);
xNSWE0 = X0; 
yNSWE0 = Y0; 

% may help speeding up computation - if N = 2^n, n >= 11
% Nseg = ceil((N/(2*256))^2);
% Nseg = ceil((N/(256))^2);

% Nseg=12;
% Nxy0=numel(X0);
% nt=length(t);
% xNSWE0 = reshape(xNSWE0, [Nxy0/Nseg Nseg]);
% yNSWE0 = reshape(yNSWE0, [Nxy0/Nseg Nseg]);

%Following line is for ode45 or ode23 etc
options=odeset('RelTol',1e-5,'AbsTol',1e-4);

tini=t(1);
tend=t(end);
tt=t(end)-t(1);
disp(['     Integration time limits are: ',num2str([tini tend]),...
    ', total: ',num2str(sign(tt)*(abs(tt)+1)),' days']) %plus one to count zero
disp(['     Tspan (data) limits are: ',num2str([tspan(1) tspan(end)]),...
    ', total: ',num2str(tspan(end)-tspan(1)+1),' days'])

% for iseg = 1:Nseg
%    xyNSWE0 = [xNSWE0(:,iseg); yNSWE0(:,iseg)];
   xyNSWE0 = [xNSWE0(:); yNSWE0(:)];

%    tic
%          [~, xyNSWE] = ode23(@uv, ...
%             t, xyNSWE0, ...
%                 options, ...
%            u, v, xspan, yspan, tspan);

            sol2 = ode23(@uv, ...
            [tini tend], xyNSWE0, ...
                options, ...
           u, v, xspan, yspan, tspan);
       xyNSWE=deval(sol2,t).';
       
% %  tic
%    sol2 = ode45(@uv, ...
%             t, xyNSWE0, ...
%                 options, ...
%            u, v, xspan, yspan, tspan);
%        xyNSWE=deval(sol2,t).';
%       %  toc
%        disp(sol.stats)

% % tic
%     xyNSWE = ode4(@uv, ...
%        t, xyNSWE0, ...
%        u, v, xspan, yspan, tspan);
% % toc

% tic
%     xyNSWE = ode5(@uv, ...
%        t, xyNSWE0, ...
%        u, v, xspan, yspan, tspan);
% toc

% disp([num2str(toc/60),' min'])
% disp(t2-t)

x = xyNSWE(:,1:size(xyNSWE,2)/2).';
y = xyNSWE(:,size(xyNSWE,2)/2+1:end).';

%    xNSWE(:,iseg,:) = xyNSWE(:,1:end/2).';
%    yNSWE(:,iseg,:) = xyNSWE(:,end/2+1:end).';
% end

% x = reshape(xNSWE, [Nxy0 nt]);
% y = reshape(yNSWE, [Nxy0 nt]);


%--------------------------------------------------------------------------
function out = uv(t, xyDxy, u, v, xspan, yspan, tspan)

n = length(xyDxy)/2;
out = zeros(2*n,1);

x = xyDxy(1:n);
y = xyDxy(n+1:2*n);

x = interp1(xspan, 1:length(xspan), x); %this is original 
y = interp1(yspan, 1:length(yspan), y); 
t = interp1(tspan, 1:length(tspan), t);


t = repmat(t, [n 1]);

% u = ba_interp3(u, x, y, t, 'cubic');
% v = ba_interp3(v, x, y, t, 'cubic');

u = interp3(u, x, y, t, 'cubic');
v = interp3(v, x, y, t, 'cubic');

out(1:n) = u;
out(n+1:2*n) = v;



