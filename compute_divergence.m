%compute_divergence

datestr(now)
clearvars
load('mean2003-2014.mat') %load climatological velocity [m/s]

% load('C:\Users\Rodrigo\Documents\Work\GoM\hor_vel_year_2014')

[xspan, yspan] = sph2xy(lon, lon(1), lat, lat(1)); %[m]


x=double(cumsum(xspan));
y=double(cumsum(yspan))';
x=x*1e-3;%m to km
y=y*1e-3;
u2=permute(u,[2 1 3]);
v2=permute(v,[2 1 3]);
u2=u2*86.4;%m/s to km/day
v2=v2*86.4;

% divergence is [1/day]
for kk=size(u,3):-1:1
divt(:,:,kk)=divergence(x,y,u2(:,:,kk),v2(:,:,kk));% divergence is Matlab built in
end

save divergence_clim