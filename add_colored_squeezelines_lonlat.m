function h=add_colored_squeezelines_lonlat...
    (dirr,filename,z,x0,y0,lon0,lat0,varargin)
% add_colored_squeezelines(dirr,filename,z,x0,y0,lon0,lat0,linewidth)
% loads LCS in file filename within directory dirr 
% adds squeezelines to current figure using colorline.m
% plotting them colored according to z which should be defined as a
% function of x0 and y0.  z is usually cRHO=cRHO(x0,y0)
% lon0 and lat0 is just some longitude and latitude value (one number) in your domain
% it is used to transform x y to lon lat. For example, use lon0=min(lonx0(:)); lat0=min(latx0(:)); 
% see also add_squeezelines add_colored_squeezelines add_squeezelines_lonlat
disp('   Begin adding squeezelines ...      =====================')
ttic=tic;
hold on
switch nargin
    case 7
        lw=1;%linewidth=1     
    case 8
        lw=varargin{1};
end

if numel(lon0)>1
    lon0=min(lon0(:));
end
if numel(lat0)>1
    lat0=min(lat0(:));
end

dirr2=pwd;
cd(dirr)

try 
aa=load(filename);
catch
   error(['Could not find mat file ',filename,' in ',dirr])
end

nLCS = size(aa.pxt,2);

ind=1; % use ind>1 if you want to skip plotting some cLCS
% xx=aa.pxt;
% yy=aa.pyt;
for kk = 1:ind:nLCS
%     xs=xx(:,kk);
%     ys=yy(:,kk);
     xs=aa.pxt(:,kk);
     ys=aa.pyt(:,kk);

    zs=interp2(x0,y0,z,xs,ys);% get crho along cLCS

    [xs, ys]=xy2sph(xs*1e3, lon0, ys*1e3, lat0);
   

  h.(['c',num2str(kk)])=colorline(xs, ys, zs,lw);
% h=colorline(x, y, zs,lw);
% uistack(h,'bottom');

end
drawnow
disp('   Done  adding squeezelines ...      =====================')
disp(['   adding squeezelines took ',num2str(toc(ttic)/60),' min'])
cd(dirr2)
