% plot_lagrangian_div_histogram
% You might need to edit the value of dd in packcols2 and 
% packrows2 to get the subplots close enough to each other while not overlapping.
clearvars
dirr=[pwd,filesep];
dirr2=[dirr,'alongpath_div',filesep];
T2=-7;% -7 is the value for the T described in paper, use same as used for compute_C
save_name=[dirr2,'expintdiv-climatology-',num2str(-T2)];
load(save_name)

mths={'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};

figure('numbertitle','off','name','alpha monthly mean')

monthi=1;
% be2= [0.999998 1.000002];
be2=[0.9999994 1.0000006];
% be2= [0.999998 1.000002]; <-- used to discern max_values
%

for monthj=[1 4 7 10 2 5 8 11 3 6 9 12]
    subplot(3,4,monthi)
    %     m_proj('lambert','long',[-98,-81],'lat',[18.1 30.7],'rectbox','on');
    %     m_pcolor(lonx0,latx0,expintdivbar.(['month_',num2str(monthj)])-1)
    
    pcolor(lonx0,latx0,expintdivbar.(['month_',num2str(monthj)])-1)
    
    colorbar off
    shading flat
    cmocean balance
    % m_usercoast('GoM_coast_i','patch',0.55*ones(1,3),'edgecolor','black');
    
    % hold on
    % m_plot(xc50,yc50,'g','Linewidth',1.15)
    caxis(be2-1)
    niceplot(16)
    if monthj ==3
        m_grid('tickdir','out','yaxisloc','left');
    else
        m_grid('tickdir','out','yticklabels','','xticklabels','');
    end
    m_text(-90.63,20,mths{monthj},'fontsize',22.5,'fontweight','bold','color',0*[1 1 1])
    drawnow
    monthi=monthi+1;
end

packboth2(3,4) %packrows--dd=0.0105;  and packcols--dd=-0.141;
colorbar('hor','Position',...
    [0.376041666666667 0.0862445414847162 0.431770833333333 0.0164699719993741],...
    'TickLength',0.014,...
    'LineWidth',2,...
    'FontWeight','bold',...
    'FontSize',16);

%
figure('numbertitle','off','name','alpha histogram for values in each month')

monthi=1;
be=[0.9999998 1.0000002]-1;
bl=diff(be)/15;
a=mantexp(be(1));
dataall=nan(12,7);
% tab=
for monthj=[1 4 7 10 2 5 8 11 3 6 9 12]
    subplot(3,4,monthi)
    
    data=expintdivMonths.(['month_',num2str(monthj)])(:)-1;
    st=stat2(data);
    dataall(monthj,:)=st.Variables;
    lb2
    disp(mths{monthj})
    disp(dataall)
    
    H=histogram(data,...
        'Normalization','probability','BinWidth',bl);%,'BinEdges',be)  ;
    hold on
    histogram(data,...
        'Normalization','cdf','BinWidth',bl,'DisplayStyle','stairs','linewidth',2)
    axis([ 2*([be(1)-eps be(2)+eps]) 0 1])
    xtv=2*(be(1)-eps:10^a:be(2)+eps);
    ind=abs(xtv)<eps*3;
    xtv(ind)=0;
    set(gca,'xTick',xtv)
    set(gca,'yTick',0:0.1:1)
    
    if monthj == 3
        % set(gca,'xTickLabel',{'0.9999994','','0.9999998','','1.0000002','','1.0000006'})
        % set(gca,'xTickLabel',{'-0.6e-6','-0.4e-6','-0.2e-6','0','0.2e-6','0.4e-6','0.6e-6'})
        for kk=1:length(xtv)
            xtxt{kk}=num2str(xtv(kk),'%.0e\n');
        end
        xtxt{ind}='0';
        set(gca,'xTickLabel',xtxt)
        % set(gca,'xTickLabelRotation',45)
        %     set(gca,'yTickLabel',{0,'',0.2,'',0.4,'',0.6,'',0.8,'',1})
        
        set(gca,'yTickLabel',{0,'',0.2,'',0.4,'',0.6,'',0.8,'',1})
        
        
        % elseif monthj == 1 || monthj == 2 || monthj == 3
        %         set(gca,'yTickLabel',{0,'',0.2,'',0.4,'',0.6,'',0.8,'',1})
        
        
        
    else
        set(gca,'xTickLabel','')
        set(gca,'yTickLabel','')
    end
    grid
    text(2*be(1)+0.5*10^a,0.65,mths{monthj})
    niceplot(17)
    drawnow
    monthi=monthi+1;
end

packboth2(3,4)

input.data=dataall;
input.tableBorders = 0;
input.tableColumnAlignment = 'c';
input.dataFormat = {'%.0e'};
input.tableRowLabels = mths;
input.tableColLabels = {'Mean','StdDev','AbsMin','1stQuartile','Median','3rdQuartile','AbsMax'};
input.tableLabel = 'stats';
% a=latexTable(input) % latexTable can be downloaded


