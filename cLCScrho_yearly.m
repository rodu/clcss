function cLCScrho_yearly(dirr)
% cLCScrho_yearly
pd=pwd;
cx=[0.4 1.3];% A suggestion is that values of 0.4 are visualized in light green so
% values less than about 0.4 are blanked, this will show crho of about 1.5 and greater

lw=1; %linewidth for cLCS might want to adjust

% choose directory from which you would like to read your results,
% for now it uses pwd as default
if nargin < 1
    dirr=[pwd,filesep];
elseif nargin==1
    dirr=[dirr,filesep];% should be ok if it gets an extra filesep
end
dirr2=[dirr,'CG',filesep,'fullyear'];

disp('=============================================================')
disp('cLCScrho_yearly using data in:               ')
disp(dirr2)
disp('=============================================================')

load([dirr2,filesep,'yearly-averaged-CG']...
    ,'sqrtlda2year','x0','y0','lonx0','latx0','count_year')
lonmin=min(lonx0(:));
latmin=min(latx0(:));
lonmax=max(lonx0(:));
latmax=max(latx0(:));
z=log(sqrtlda2year/count_year);

figure

% % use following lines if not using m_map (colored lines) ================================
% % colored squeezelines
% add_colored_squeezelines_lonlat(dirr2,'cLCS_yearly'...
%     ,z,x0,y0,lonmin,latmin,lw)
% %========================================================================


% % use following lines if not using m_map (black lines) ================================
% % black squeezelines
% add_squeezelines_lonlat(dirr2,'cLCS_yearly'...
%     ,lonmin,latmin,lw)
% %========================================================================


% % use following lines if  using m_map ====================================
m_proj('lambert','long',[lonmin,lonmax],'lat',[latmin latmax]... % choose your projection here
    ,'rectbox','on');
add_colored_squeezelines(dirr2,'cLCS_yearly'...
    ,z,x0,y0,lonmin,latmin,lw) %,x0,y0,lon0,lat0,varargin
% m_usercoast('GoM_coast_i','patch',0.55*[1 1 1],'edgecolor','black');
m_coast('patch',0.55*[1 1 1],'edgecolor','black');
% m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
m_grid('tickdir','out','yaxisloc','left');
colorbar
colormap shadden
caxis(cx)
% %========================================================================
cd(pd)
disp('=============================================================')
