function crho_yearly(dirr)
% crho_yearly
cx=[0.4 1.3];% A suggestion is that values of 0.4 are visualized in light green so
% values less than about 0.4 are blanked, this will show crho of about 1.5 and greater

% choose directory where you would like to read your results, for now it
% uses pwd as default
if nargin < 1
    dirr2=[pwd,filesep];
elseif nargin==1
    dirr2=[dirr,filesep];% should be ok if it gets an extra filesep
end

disp('=============================================================')
disp('crho_yearly using data in:               ')
disp(dirr2)
disp('=============================================================')

load([dirr2,filesep,'yearly-averaged-CG']...
    ,'sqrtlda2year','lonx0','latx0','Tvec1')
lonmin=min(lonx0(:));
latmin=min(latx0(:));
lonmax=max(lonx0(:));
latmax=max(latx0(:));
z=log(sqrtlda2year/length(Tvec1));


m_proj('lambert','long',[lonmin,lonmax],'lat',[latmax latmin],'rectbox','on');
m_pcolor(lonx0,latx0,z)
shading flat
% m_usercoast('GoM_coast_i','patch',0.55*[1 1 1],'edgecolor','black');
m_coast('patch',0.55*[1 1 1],'edgecolor','black');
% m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
m_grid('tickdir','out','yaxisloc','left');
colormap shadden
% colormap bone
caxis(cx)