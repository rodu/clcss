function [mant,expnt] = mantexp(arg)
% [mant,expnt] = mantexp(arg)
% Returns the mantissa and exponent of a real base 10 argument.
% TEST DATA:
% arg = (0.5 - rand) * 10^fix(10*(0.5-rand))
% sprintf('\n\t%23.15E\n',arg)

sgn = sign(arg);
expnt = fix(log10(abs(arg)));
mant = sgn .* 10.^(log10(abs(arg))-expnt);
% if abs(expnt) < 1
%     expnt = expnt * 10;
%     mant = mant - 1;
% end



% sprintf('\n\t%6.4f x E%+04d\n',mant,expnt)

return % Added to indicate end of function
