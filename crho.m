function crho(monthvec,dirr)
% crho(monthvec)
% this plots cRHO
% change directory so dirr matches where you monthly directories are (line 13)
% monthvec is a vector of all months for which you want to plot cRHO
% Example:
% for kk=1:12
%     crho(kk)
% end
cx=[0.4 1.3]; % A suggestion is that values of 0.4 are visualized in light green so
% values less than about 0.4 are blanked, this will show crho of about 1.5 and greater
% T2=-7;
% dirr='C:\Users\Rodrigo\OneDrive\transit\clcss';      
for monthnum=monthvec
dirr2=[dirr,filesep,'CG',filesep,num2str(monthnum)];

disp('=============================================================')
disp('crho is using data in:                   ')
disp(dirr2)
disp('=============================================================')
load([dirr2,filesep,'TOT-',num2str(monthnum)],'lonx0','latx0','Tvec1','sqrtlda2TOT')
lonmin=min(lonx0(:));
latmin=min(latx0(:));
lonmax=max(lonx0(:));
latmax=max(latx0(:));
figure
m_proj('lambert','long',[lonmin,lonmax],'lat',[latmax latmin]...
    ,'rectbox','on');
z=log(sqrtlda2TOT/length(Tvec1));
% [lonx0,latx0]=meshgrid(lonx0,latx0);
m_pcolor(lonx0,latx0,z)
shading flat
% m_usercoast('GoM_coast_i','patch',0.55*[1 1 1],'edgecolor','black');
m_coast('patch',0.55*[1 1 1],'edgecolor','black');
% m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
m_grid('tickdir','out','yaxisloc','left');
colormap shadden
% colormap bone

caxis(cx)
disp('=============================================================')

end
