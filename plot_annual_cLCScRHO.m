function plot_annual_cLCScRHO(dirr)
% plot_annual_cLCScRHO.m replicates figure 2 of paper 
% found at https://www.nature.com/articles/s41598-018-23121-y
% but with your own data.
% You might need to edit the value of dd in packcols2 and 
% packrows2 to get the subplots close enough to each other while not overlapping. 


mths={'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};

if nargin < 1
    dirr=[pwd,filesep,'CG',filesep];
elseif nargin==1
    dirr=[dirr,filesep,'CG',filesep];% should be ok if it gets an extra filesep
end


figure
monthi=1;
datestr(now)
tic
cx=[0.4 1.3];% A suggestion is that values of 0.4 are visualized in light green so
% values less than about 0.4 are blanked, this will better to show larger crho
% cx=[0.1 1];
lw=0.55;
% T2=-7;
for month=[1 4 7 10 2 5 8 11 3 6 9 12] %each column is roughly a season
    monthnum=num2str(month);
    lb2
    dirr2=[dirr,filesep,num2str(monthnum)];
    disp('=============================================================')
    disp('             plot_annual_cLCScRHO using data in:             ')
    disp(dirr2)
    disp('=============================================================')
    load([dirr2,filesep,'TOT-',num2str(monthnum)],'lonx0','latx0','lda2TOT','Tvec1')%
    lonmin=min(lonx0(:));%
    latmin=min(latx0(:));%
    lonmax=max(lonx0(:));
    latmax=max(latx0(:));
    [x0, y0]=sph2xy(lonx0, lonmin, latx0, latmin);

    disp(dirr2)
    disp(monthnum)
    subplot(3,4,monthi)

    m_proj('lambert','long',[lonmin,lonmax],'lat',[latmax latmin]...
        ,'rectbox','on');
    % choose z =================================
    % z=ftleTOT/length(Tvec1);  %ftleTOT has been divided by 2T already and it is the log of lda2 already: ftle=log(lda2)/(2*abs(T2));
    z=log(lda2TOT/length(Tvec1));%lda2TOT=lda2TOT+lda2; where lda2 = real(.5*trC + sqrt(.25*trC.^2 - detC)); is the largest eigenvalue of CG

    add_colored_squeezelines(dirr2,['cLCS_',num2str(monthnum)]...
        ,z,x0,y0,lonmin,latmin,lw); %,x0,y0,lon0,lat0,varargin
    % m_usercoast('GoM_coast_i','patch',0.55*[1 1 1],'edgecolor','black');
    m_coast('patch',0.55*[1 1 1],'edgecolor','black');
    % m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
    m_grid('tickdir','out','yaxisloc','left');
    colormap shadden
    % colormap bone


    caxis(cx)

    if month == 3%2
        m_grid('tickdir','out','yaxisloc','left','FontSize',22,'FontWeight','bold');
    else
        m_grid('tickdir','out','yticklabels','','xticklabels','');
    end

    m_text(lonmin+.5,latmin+.5,mths{month},'fontsize',22.5,'fontweight','bold','color',0*[1 1 1])
    drawnow
    monthi=monthi+1;
end
packboth2(3,4) %packrows--dd=0.0105;  and packcols--dd=-0.141;
% colorbar_fun
hh=colorbar('hor','Position',...
    [0.385 0.0862445414847162 0.431770833333333 0.0164699719993741],...
    'Ticks',[0.2 0.4 0.6 0.8 1 1.2 1.4],...
    'FontWeight','bold',...
    'FontSize',16);
%     [0.838633340391104 0.172489082969432 0.00772082627556292 0.68122270742358],...
if min(cx) < 0.5
    % hh.Ticks=[min(cx):0.1:max(cx)];
    % hh.TickLabels={({hh.Ticks})};
    hh.Ticks=0.4:0.1:1.3;
else
    hh.Ticks=[ 0.5 0.75 1 1.25 1.5];
    hh.TickLabels={ '0.5' '0.75' '1' '1.25' '1.5'};
end

hh.TickDirection='in';
hh.TickLength=0.013;
hh.LineWidth=1.75;

toc

