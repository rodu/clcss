% run_everything_cLCS
% this code computes a climatology, then time-averaged CG tensors
% then monthly squeezelines and yearly squeezelines
% please read instructions in README.md first
% this way you can set up the data you will need
% in the directory (or directories) as needed

tt=tic;
dirr{1}='C:\mydir\ismydir\';
% % if you would like to compute cLCS for more that one
% % dataset, keep entering directories as exemplified below,
% dirr{2}='C:\myotherdir\isalsomydir\';
% dirr{3}='C:\youget\theidea\';

Nd=length(dirr);
nwaves=3.5;
monthvec=7:9;%1:12;
for kk=1:Nd
    dirr2=dirr{kk};
    diary(['diary_run_everything_cLCS_dirr_',num2str(kk),'.txt']) % you can comment this, do help diary
    create_clim(dirr2)%netcdf files with data should be in dirr; this step skipped if hycom_clim exists already
    meanC(monthvec,dirr2)% doing all 12 months here
    compute_squeeze_lines(monthvec,dirr2)
    compute_squeeze_lines_yearly(dirr2)
    cLCS_vis(dirr2,nwaves,monthvec)
    disp(' ')
    disp(['========================================================================',newline])
    disp(['          =========== done with directory: ============',newline])
    disp([dirr2,newline])
    if kk< Nd
        disp(['           ======== on to the next directory: =========',newline])
        disp([dirr{kk+1},newline])
    elseif kk==Nd
        disp(['           ======== FINISHED ALL DIRECTORIES! =========',newline])
        disp(['           ======== If you would like to plot use crho, cLCScrho, crho_yearly, cLCScrho_yearly, plot_annual_cLCScRHO',newline])
    end
    disp(['========================================================================',newline])
    diary off % you can comment this, do help diary
end
tt=toc(tt);
disp(['Total time for data in directories ',newline]) 
dirr{:}%#ok
disp(['was ',num2str(tt/60),' minutes'])
disp(['========================================================================',newline])