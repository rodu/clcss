% plot_monthly_mean_div
% You might need to edit the value of dd in packcols2 and 
% packrows2 to get the subplots close enough to each other while not overlapping.
clearvars

load('divergence_clim')

% compute monthly means
cnt=0;
for kk=1:30:360
    cnt=cnt+1;
    tvec=kk:kk+29;
    disp(tvec([1 end]))
    div.(['m',num2str(cnt)])=nanmean(divt(:,:,tvec),3);
    
end

mths={'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};

figure('numbertitle','off','name','monthly div')
oversecond2overday=86400;%factor to convert from 1/s to 1/day, change to one if you prefer 1/s
monthi=1;
for monthj=[1 4 7 10 2 5 8 11 3 6 9 12]  
    subplot(3,4,monthi)
    % use next line if usng m_map and comment pcolor below
    %     m_proj('lambert','long',[-98,-81],'lat',[18.1 30.7],'rectbox','on');
    %     m_pcolor(lon,lat,div.(['m',num2str(monthj)]))
    
    pcolor(lon,lat,div.(['m',num2str(monthj)])*oversecond2overday)
    
    shading flat
    colorbar off
    cmocean balance % download from https://www.mathworks.com/matlabcentral/fileexchange/57773-cmocean-perceptually-uniform-colormaps
    caxis(1.5e-8*[-1 1]*oversecond2overday)
    
    % use next line if usng m_map
    % m_coast('patch',0.655*[1 1 1],'edgecolor','none');
    
    
    % % to plot a contour etc use:
    % hold on
    % m_plot(xc50,yc50,'g','Linewidth',0.9)
    
    if monthj == 3
        % use next line if usng m_map
        % m_grid('tickdir','out','yaxisloc','left');
    else
        set(gca,'YTickLabel','','XTickLabel','')
        % use next line if usng m_map
        %  m_grid('tickdir','out','yticklabels','','xticklabels','');
    end
    
    % use next line if usng m_map
    %     m_text(-90.63,20,mths{monthj},'fontsize',22.5,'fontweight','bold','color',0*[1 1 1])
    text(-90.63,20,mths{monthj},'fontsize',22.5,'fontweight','bold','color',0*[1 1 1])
    
    drawnow
    monthi=monthi+1;
end

packboth2(3,4) %packrows--dd=0.0105;  and packcols--dd=-0.141;
colorbar('hor','Position',...
    [0.376041666666667 0.0862445414847162 0.431770833333333 0.0164699719993741],...
    'TickLength',0.014,...
    'LineWidth',2,...
    'FontWeight','bold',...
    'FontSize',16);

figure('numbertitle','off','name','yearly div')
% use next line if usng m_map and comment pcolor below
%     m_proj('lambert','long',[-98,-81],'lat',[18.1 30.7],'rectbox','on');
%     m_pcolor(lon,lat,nanmean(divergence,3))
pcolor(lon,lat,nanmean(divergence,3))
shading flat
cmocean balance % download from https://www.mathworks.com/matlabcentral/fileexchange/57773-cmocean-perceptually-uniform-colormaps
caxis(1.5e-8*[-1 1]*oversecond2overday)
%     hold on
% m_plot(xc50,yc50,'g','Linewidth',0.9)
    
    
