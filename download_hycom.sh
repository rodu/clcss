#! /bin/bash

#lon,-98.0,-80.0 -d lat,24.0,30.8
# for GoM hycom global I used: -97.84,-81.12 18.0,30.96
lonRange=$1
latRange=$2

c1=0
c2=0

for year in {1994..2015}
do
    outFile=uv_$year.nc
    
    if [ -f "$outFile" ]; then
        echo "$outFile exists"
		((c1=c1+1))
    else
		((c2=c2+1))
        echo "Retrieving $year =================="
        echo "lon $lonRange"
        echo "lat $latRange"
        ncks -d time,"$year-01-01 00:00:00","$year-12-31 23:00:00",8 -d lon,$lonRange -d lat,$latRange -d depth,0.0 -v water_v,water_u http://tds.hycom.org/thredds/dodsC/GLBv0.08/expt_53.X/data/$year $outFile
    fi
	
done
echo $c1 files have been found locally
echo $c2 files have been downloaded




