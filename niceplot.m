function niceplot(ftsize,inout,c)
% niceplot(ftsize,inout,c)
%NICEPLOT  Create a plot with big, bold lettering - presentation/thesis quality
%   NICEPLOT makes the title,x/y labels and user supplied text big 
%   and bold using the default color black, the x/y ticklabels big
%   and bold, thickens the axis border, and turns the ticks out on 
%   the current plot.
% inout is a string to determine if ticks are 'out' or 'in'
%ftsize is default 16 and color is default black
%

%   Mike Cook - NPS Oceanography Dept.,  MAR 94
%   Modified by Mike Cook, APR 94 to make any user supplied text
%   big and bold also.
%   Mike Cook, AUG 95, modified to allow user supplied fontsize.
%   Mike Cook, OCT 95, default font size changed from 18 to 14 point.
%   Mike Cook, OCT 95, bug fix. Complained with nice'ing a pcolor'ed
%   plot.  Changed line if tx == 'text' to if strcmp(tx,'text'). This
%   fixed the problem.


%     if nargin < 3
%         lnw=1.4;
%     end

    if nargin < 3
            c = 'k';
%         lnw=1.4;
    end
    if nargin < 1
            ftsize = 16;
    end

    if nargin < 2
            inout = 'out';
    end
    
    if ~ischar(c)
        error(' The input color must be a string ')
    end

%		Get all children of the current axis.
	chldren = get(gca,'Children');
	[row,~] = size(chldren);

%     set(get(gca,'Title'),'Color',c,'FontName','Helvetica', ...
%         'FontSize',ftsize,'FontWeight','bold')
%     set(get(gca,'Xlabel'),'Color',c,'FontName','Helvetica', ...
%         'FontSize',ftsize,'FontWeight','bold')
%     set(get(gca,'Ylabel'),'Color',c,'FontName','Helvetica', ...
%         'FontSize',ftsize,'FontWeight','bold')
% 	set(gca,'FontName','Helvetica','FontSize',ftsize,'FontWeight', ...
% 	    'bold','LineWidth',2,'TickDir',inout,'TickLength',[.004 .015])

 set(get(gca,'Title'),'FontName','Helvetica', ...
        'FontSize',ftsize,'FontWeight','bold')
    set(get(gca,'Xlabel'),'FontName','Helvetica', ...
        'FontSize',ftsize,'FontWeight','bold')
    set(get(gca,'Ylabel'),'FontName','Helvetica', ...
        'FontSize',ftsize,'FontWeight','bold')
	set(gca,'FontName','Helvetica','FontSize',ftsize,'FontWeight', ...
	    'bold','LineWidth',1.25,'TickDir',inout,'TickLength',[.004 .004])



    %[.0105 .050]
%     	set(gca,'FontName','Helvetica','FontSize',ftsize,'FontWeight', ...
% 	    'bold','LineWidth',1,'TickDir','out') %this is original

set(gca,'linewidth',1.75)
    
%	Check for any "text" type children
%	and make them all big and bold helvetica text.	    
if row > 0
	       for i = 1:row
	       tx = get(chldren(i),'Type');
	         if strcmp(tx,'text')  
%                if tx == 'text' %This is bad if tx = 'patch', tx is 1x5 and 'text' is 1x4.
		       set(chldren(i),'Color',c,'FontName','Helvetica', ...
		          'FontSize',ftsize,'FontWeight','bold')
           end
        end
end
box on
end

