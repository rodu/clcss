function cLCS_vis(dirr,nwaves,monthvec)
% cLCS_vis(dirr,nwaves,monthvec)

ticstart=tic;
if nargin <2
    nwaves=1.75;
    monthvec=1:12;
end
FR=1; %FrameRate for movie, frames per second

g = @(x,y) sin(x*nwaves).*sin(y*nwaves);


mths={ 'Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Oct' 'Nov' 'Dec'};
velname=[dirr,filesep,'hycom_clim'];
load(velname,'u','v','time','lon','lat') %load climatological velocity with variables: lon lat u v time
assert(isvector(lon) & isvector(lat),'lon and lat should be vectors, not arrays')

if iscolumn(lon) %lon lat must be row vectors
    lon=lon.';
end
if iscolumn(lat)
    lat=lat.';
end



lon=double(lon);lat=double(lat); %hycom writes lon lat in single precision, need double.
lonmin=min(lon);
lonmax=max(lon);
latmin=min(lat);
latmax=max(lat);

[m,n,p]=size(u);

if m==length(lon)
    if m==n
        warning("First and second dimenions are the same, can't distinguish whether permute is necessary or not")
    end
    whos u v lon lat time
    u=permute(u,[2 1 3]); %vel should have dimensions Ny x Nx x Nt
    v=permute(v,[2 1 3]); %vel should have dimensions Ny x Nx x Nt
    warning('=========== permuted velocity, it should have dimensions Ny x Nx x Nt  ============')
    whos u v lon lat time
end

%prepare boundary conditions
% numBC=3;
% u(1:numBC,:,:)=0;v(1:numBC,:,:)=0; %south
% u(end-numBC:end,:,:)=0;v(end-numBC:end,:,:)=0;%north
%
% u(:,1:numBC,:)=0;v(:,1:numBC,:)=0; %west
% u(:,end-numBC:end,:)=0;v(:,end-numBC:end,:)=0;%east
disp(' ')
disp('    =========== Assuming velocity is in meters/second ============')
disp(' ')
disp('========================================================================')
u=u*86.4; % m/s --> km/day
v=v*86.4;
inan=isnan(u+v);
u(inan)=0;
v(inan)=0;

% need lon lat from velocity
% need lonx0 latx0 for trajectories see C:\Users\Rodrigo\OneDrive\transit\work\trajectories_with_cLCS_red_sea.m
% absmax=max(lon(:));
% absmin=min(lon(:));
lx=[lonmin, lonmax];
% absmax=max(lat(:));
% absmin=min(lat(:));
ly=[latmin, latmax];
dx=0.04;
dy=0.04;
[x0,y0]=meshgrid(lx(1):dx:lx(2),ly(1):dy:ly(2));
[Nx,Ny]=size(x0);

if p==365%365 day climatology
    endmonth =cumsum([31    28    31    30    31    30    31    31    30    31    30    31]);
    beginmonth=[1 endmonth(1:end-1)+1 ];
    t365=true;
    t360=false;
    disp('=========== 365 day climatology, monthly indices are: ============')
elseif p==360
    t360=true;
    t365=false;
    disp('=========== 360 day climatology, monthly indices are: ============')
end

figure
ind=0;
for  nmonth=monthvec
    ind=ind+1;
    disp(['ind is ',num2str(ind),' monthvec is ',num2str(monthvec)])
% mname=[velname,'_months_',num2str(monthvec(1)),'_',num2str(monthvec(end)),'_cLCS_deformation.mp4'];
mname=[velname,'_month_',num2str(monthvec(ind)),'_cLCS_deformation.mp4'];
vidfile = VideoWriter(mname,'MPEG-4');%#ok
vidfile.Quality=95;
vidfile.FrameRate=FR;
open(vidfile);



    if t360
        tind=(1:30)+30*(nmonth-1);
        int_times(1,:)=tind(15):-1:tind(1);
        int_times(2,:)=tind(30):-1:tind(16);
    elseif t365
        tind=beginmonth(nmonth):endmonth(nmonth);
        try
            int_times(1,:)=tind(15):-1:tind(1);
            int_times(2,:)=tind(30):-1:tind(16);
        catch
            int_times(1,:)=tind(14):-1:tind(1);
            int_times(2,:)=tind(28):-1:tind(15);
        end
    end

    disp(tind)
    lb
    disp(' ')




    yearind=1;
    t0=int_times(yearind,1);
    tf=int_times(yearind,end);
    tic
    [lont,latt,~]=trajectories(lon,lat,time(tind),u(:,:,tind),...
        v(:,:,tind),x0,y0,t0,tf,int_times(yearind,:));
    toc
    yearind=2;
    t02=int_times(yearind,1);
    tf2=int_times(yearind,end);
    tic
    [lont2,latt2,~]=trajectories(lon,lat,time(tind),u(:,:,tind),...
        v(:,:,tind),x0,y0,t02,tf2,int_times(yearind,:));
    toc

    %     trajt=sort(trajt);
    %     trajt2=sort(trajt2);

    Nt=size(latt,2);
    lont = reshape(lont,Nx, Ny, Nt);
    latt = reshape(latt,Nx, Ny, Nt);
    lont2 = reshape(lont2,Nx, Ny, Nt);
    latt2 = reshape(latt2,Nx, Ny, Nt);

    for kk=Nt:-1:1
        G(:,:,kk)=g(lont(:,:,kk),latt(:,:,kk));
        G2(:,:,kk)=g(lont2(:,:,kk),latt2(:,:,kk));
    end

    %get rid of trajectories that do not move
    ii=((lont(:,:,end)-lont(:,:,1))==0);
    g0=g(x0,y0);
    g0(ii)=NaN;
    ii=repmat(ii,[1 1 Nt]);
    G(ii)=NaN;
    G2(ii)=NaN;
    x=x0(1,:);
    y=y0(:,1);

    casse='one_plot';
    %     casse='two_subplot';
    switch casse
        case 'two_subplot'
            clf
            subplot(121)
            m_proj('lambert','long',[lonmin,lonmax],'lat',[latmin latmax]... % choose your projection here
                ,'rectbox','on');
            m_pcolor(x,y,g0), shading flat
            colormap('thompson(16)')
            axis image
            ax=axis;
            niceplot
            text(44,28,mths{nmonth},'FontSize',20,'FontWeight','bold')
            add_squeezelines(dirr,nmonth)
            m_coast('patch',0.55*[1 1 1],'edgecolor','black');
            % m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
            m_grid('tickdir','out','yaxisloc','left');

            subplot(122)
            m_proj('lambert','long',[lonmin,lonmax],'lat',[latmin latmax]... % choose your projection here
                ,'rectbox','on');
            m_pcolor(x,y,g0), shading flat
            colormap('thompson(16)')
            axis image
            axis(ax)
            niceplot
            add_squeezelines(dirr,nmonth)
            m_coast('patch',0.55*[1 1 1],'edgecolor','black');
            m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
            %     m_grid('tickdir','out','yaxisloc','left');
            drawnow
            packfig(1,2,'cols')%dd=0.01
            %     pause


            F = getframe(gcf);
            vidfile.writeVideo(F);



            for kk=1:Nt
%                 ind=ind+1;
                clf
                subplot(121)
                m_proj('lambert','long',[lonmin,lonmax],'lat',[latmin latmax]... % choose your projection here
                    ,'rectbox','on');
                m_pcolor(x,y,G(:,:,kk)), shading flat
                colormap('thompson(16)')
                axis image
                axis(ax)
                niceplot
                text(44,28,mths{nmonth},'FontSize',20,'FontWeight','bold')
                add_squeezelines(dirr,nmonth)
                m_coast('patch',0.55*[1 1 1],'edgecolor','black');
                % m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
                m_grid('tickdir','out','yaxisloc','left');

                subplot(122)
                m_proj('lambert','long',[lonmin,lonmax],'lat',[latmin latmax]... % choose your projection here
                    ,'rectbox','on');
                m_pcolor(x,y,G2(:,:,kk)), shading flat
                colormap('thompson(16)')
                axis image
                axis(ax)
                niceplot
                add_squeezelines(dirr,nmonth)
                m_coast('patch',0.55*[1 1 1],'edgecolor','black');
                m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
                %         m_grid('tickdir','out','yaxisloc','left');
                drawnow
                packfig(1,2,'cols')%dd=0.01
                drawnow

                F = getframe(gcf);
                vidfile.writeVideo(F);
                %         writeVideo(vidfile,F(ind));
                %         pf2([num2str(nmonth),'_adv_',num2str(t0),'_',num2str(trajt(kk)),'_',num2str(t02),'_',num2str(trajt2(kk))],2,figdirr)
            end

        case 'one_plot'
            clf
            m_proj('lambert','long',[lonmin,lonmax],'lat',[latmin latmax]... % choose your projection here
                ,'rectbox','on');
            m_pcolor(x,y,g0), shading flat
            colormap('thompson(16)')
            axis image
            ax=axis;
            niceplot
            text(44,28,mths{nmonth},'FontSize',20,'FontWeight','bold')
            add_squeezelines(dirr,nmonth)
            m_coast('patch',0.55*[1 1 1],'edgecolor','black');
            % m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
            m_grid('tickdir','out','yaxisloc','left');
            G=cat(3,G,G2);
            for kk=1:2*Nt
%                 ind=ind+1;
                clf
                m_proj('lambert','long',[lonmin,lonmax],'lat',[latmin latmax]... % choose your projection here
                    ,'rectbox','on');
                m_pcolor(x,y,G(:,:,kk)), shading flat
                colormap('thompson(16)')
                axis image
                axis(ax)
                niceplot
                text(44,28,mths{nmonth},'FontSize',20,'FontWeight','bold')
                add_squeezelines(dirr,nmonth)
                m_coast('patch',0.55*[1 1 1],'edgecolor','black');
                % m_grid('tickdir','out','yaxisloc','left','xticklabel','','yticklabel','');
                m_grid('tickdir','out','yaxisloc','left');
                drawnow

                F = getframe(gcf);
                vidfile.writeVideo(F);
                %         writeVideo(vidfile,F(ind));
                %         pf2([num2str(nmonth),'_adv_',num2str(t0),'_',num2str(trajt(kk)),'_',num2str(t02),'_',num2str(trajt2(kk))],2,figdirr)
            end

    end
close(vidfile)
disp('=============================================================================')
disp(' ')
disp('         ===========  cLCS_vis just saved a movie  ============')
disp('look for: ')
disp(mname)
disp(' ')
end
disp(' ')
disp(['Total cLCS_vis time was ',num2str(toc(ticstart)/60),' minutes'])
disp(' ')
disp('=============================================================================')

end

function [lont,latt,txy]=trajectories(lon,lat,tspan,u,v,lon0,lat0,t0,tf,t)

lon_origin=min(lon(:)); lat_origin=min(lat(:));
[xspan, yspan] = sph2xy(lon, lon_origin, lat, lat_origin);
xspan=xspan*1e-3; %[km]
yspan=yspan*1e-3; %[km]
[x0,y0]=sph2xy(lon0, lon_origin, lat0, lat_origin);
x0=x0*1e-3; %[km]
y0=y0*1e-3; %[km]


% disp(['    Integration time limits are: [',datestr(t0),', ',datestr(tf),'], total: ',num2str(sign(dt(1))*length(t0:sign(dt):tf)),' days'])
% disp(['    tspan (time for data) limits are: [',datestr(tspan(1)),', ',datestr(tspan(end)),'], total: ',num2str(length(tspan(1):tspan(end))),' days'])

ttt=tic;
xy0 = [x0(:); y0(:)];
options=odeset('RelTol',1e-5,'AbsTol',1e-4);%

sol2 = ode23(@uv, ...
    [t0 tf],xy0,options,...
    u, v, xspan, yspan, tspan);
xy=deval(sol2,t).';
txy=t;

disp(['    ===  === Trajectory integration took ',num2str(toc(ttt)/60),' min'])

x = xy(:,1:end/2).';
y = xy(:,end/2+1:end).';

[lont, latt]=xy2sph(x*1e3, lon_origin, y*1e3, lat_origin);%return to lonlat

end

function out = uv(t, xyDxy, u, v, xspan, yspan, tspan)

n = length(xyDxy)/2;
out = zeros(2*n,1);

x = xyDxy(1:n);
y = xyDxy(n+1:2*n);

x = interp1(xspan, 1:length(xspan), x,'linear'); % mapping to index-space
y = interp1(yspan, 1:length(yspan), y,'linear'); %
t = interp1(tspan, 1:length(tspan), t,'linear');

t = repmat(t, [n 1]);

%  u = ba_interp3(u, x, y, t, 'cubic');
%  v = ba_interp3(v, x, y, t, 'cubic');

u = interp3(u, x, y, t, 'cubic',0);
v = interp3(v, x, y, t, 'cubic',0);

out(1:n) = u;
out(n+1:2*n) = v;
end

function [lambda, theta]=xy2sph(x, lambda0, y, theta0)
%XY2SPH Curvilinear spherical to spherical.
% [lon, lat] = xy2sph(X,lon0,Y,lat0) where X,Y are in meters
% and lon0,lat0 are in degrees.
R = 6371 * 1e3;
dg2rad = pi/180;
lambda = lambda0 + x/(R*cos(theta0*dg2rad)) / dg2rad;
theta = theta0 + y/R / dg2rad;
end

function [x, y] = sph2xy(lambda, lambda0, theta, theta0)
%SPH2XY Spherical to curvilinear spherical.
%[x, y] = sph2xy(lon, lon_origin, lat, lat_origin) where x,y are in meters
%and lon's ,lats's are in degrees.
R = 6371 * 1e3;
dg2rad = pi/180;
x = R * (lambda - lambda0)*dg2rad * cos(theta0*dg2rad);
y = R * (theta - theta0)*dg2rad;
end
