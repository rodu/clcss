function [C11,C22,C12,x0,y0,lon0,lat0]=...
    compute_C_par(lon,lat,u,v,T,tspan,save_dirr)
disp([ '       ========================================================================= ';...
    '       =============== compute_C_par began  ',datestr(now),' =============== ';...
     '       ========================================================================= '])
% [C11,C22,C12,x0,y0,lon0,lat0]=compute_C_par(lon,lat,u,v,T,tspan,save_dirr)
% lon = 1xNx    lat= 1xNy everything double
% tspan=1xNt  
% u v should be Ny x Nx x Nt units should be km/day 
%
% Nota que cada component de la velocidad se supone que esta
% arreglada tridimensionalmenete: Ny x Nx x Nt.  Al menos diariamente.  Un
% paso de tiempo de 0.1 dias funciona bien con Runge-Kutta.  Pero mejor es
% ode45 (paso variable) pero puede tardar mucho.  Lo otro a tener en mente
% es la dimension de la grilla.  Lo escribi demanera que un da una
% potencia de 2 (512 es un poco coarse para todo el GoM; 1024 es lo mas
% apropiado; mas grande tarda mucho y es para calculas de LCS precisos).
% Despues divido la grilla en pedazos y advecto por separado estos
% pedazos.  Esto es mas rapido que hacer todo de un saque.  Hasta grillas
% de 256 individuales hay ventaja.  Mas grande es lento.  Mas chico el
% loop es mas largo y por tanto mas lento tambien.  
% Adicionalmente, este codigo ahora esta parallelizado usa un loop parfor.
% para correr en varios procesadores, primero correr el comando parpool 
%
% If you are using a for loop instead of the parfor loop parfor --> for in
% line 120




 lon0min = min(lon(:));                                                           % [deg]
 lon0max = max(lon(:));                                                           % [deg]
 lat0min = min(lat(:));                                                           % [deg]
 lat0max = max(lat(:));                                                           % [deg]


lon_origin=lon0min; lat_origin=lat0min;
% [x, y] = sph2xy(lambda, lambda0, theta, theta0)
[x0min, y0min] = sph2xy(lon0min, lon_origin, lat0min, lat_origin);
[x0max, y0max] = sph2xy(lon0max, lon_origin, lat0max, lat_origin);
x0min = x0min*1e-3;                                                        % [km]
y0min = y0min*1e-3;                                                        % [km]
x0max = x0max*1e-3;                                                        % [km]
y0max = y0max*1e-3;   % [km]

[xspan, yspan] = sph2xy(lon, lon_origin, lat, lat_origin);
xspan=xspan*1e-3; %[km]
yspan=yspan*1e-3; %[km]
% [xx, yy] = meshgrid(xspan,yspan);


% % Grid for CG tensor
% % The higher the value of N the greater the resolution, 1024 is good for
% % the Gulf of Mexico which is roughly 1200x1600 km
% N = 1024;
% % N=N/2;
% % N=N*2; % this doubles the resolution of the C tensor, 
% % it will obviously be slower to compute. 
% fac = (x0max-x0min)/abs(y0max-y0min);
% if fac > 1 % wider rather than tall
%    Nx0 = N;
%    Ny0 = fix(N/fac);
% else % either taller or square
%    Ny0 = N;
%    Nx0 = fix(N*fac);
% end

% Alternatively just choose 2 points in computational grid per velocity grid point
% Note that Nx0 and Ny0 should be an even number when using parallel
% computations
Nx0=numel(xspan)*2;
Ny0=numel(yspan)*2;

disp(['       =========     Your initial condition grid for CG is '...
    ,num2str(Ny0),' by ',num2str(Nx0),'     ========='])


Nxy0 = Nx0*Ny0;
x0 = linspace(x0min, x0max, Nx0); 
y0 = linspace(y0min, y0max, Ny0);
[lon0,lat0]=xy2sph(x0*1e3, lon_origin, y0*1e3, lat_origin);
[X0, Y0] = meshgrid(x0, y0);
% debugging purposes:
% whos X0 Y0
% plot(xx,yy,'bo')
% hold on
% plot(X0,Y0,'r.')

% computational (x,y) grid
X0 = X0(:);
Y0 = Y0(:);
dx0 = .1; %.1*mean(diff(x0));
dy0 = .1; %.1*mean(diff(y0));
xNSWE0 = [X0 X0 X0-dx0 X0+dx0]; %initial positions +- deltax
xNSWE0 = xNSWE0(:);
yNSWE0 = [Y0+dy0 Y0-dy0 Y0 Y0]; %initial positions +- deltay
yNSWE0 = yNSWE0(:);

% may help speeding up computation - if N = 2^n, n >= 11
% note Nseg should be an even number
% Nseg = ceil((N/(2*256))^2);
% Nseg = ceil((N/(256))^2);
% Nseg=4;
Nseg=12; % for parallel loop, make Nseg something around 12-16 and a multiple of the number of processors
%being used. So for example if number of processors is 4 then Nseg=12 is a
%good option (4*3=12). However, if you have 4 processors but are low on RAM
%memory (less than 16Gb or so), maybe Nseg=16 is better choice since each
%iteration works on smaller arrays. For 32 processors try Nseg=64 or if your
%domain is not too big (or if you have a lot of RAM) try Nseg=32 . 
% It will need less RAM with Nseg=64 or even Nseg=96;
disp(['       Nseg is ',num2str(Nseg)])
% you will need Nxy0 to be divisible by Nseg 
% use matlab's factor function to see how Nxy0 factors
% and make sure Nseg is a mutiple of some of those factors
% while remaining a factor of 2
xNSWE0 = reshape(xNSWE0, [4*Nxy0/Nseg Nseg]);%stencil for derivative has 4 points
yNSWE0 = reshape(yNSWE0, [4*Nxy0/Nseg Nseg]);

% Cauchy-Green tensor
t0=tspan(end);
t = t0:sign(T)*.1:tspan(1); %[t0+T t0]; length(t0+T:t0),
xNSWE = zeros(4*Nxy0/Nseg, Nseg);
yNSWE = xNSWE;
%Following 3 lines are for ode45 or ode23, not needed if using ode2 or some
%other simple ode function
options=odeset('RelTol',1e-5,'AbsTol',1e-4);%
tini=t(1);
tend=t(end);

tt=t(end)-t(1);
disp(['     Integration time limits are: ',num2str([tini tend]),...
    ', total: ',num2str(sign(tt)*(abs(tt)+1)),' days']) %plus one to count zero
disp(['     Tspan (data) limits are: ',num2str([tspan(1) tspan(end)]),...
    ', total: ',num2str(tspan(end)-tspan(1)+1),' days'])

tt=tic;
parfor iseg = 1:Nseg % parfor is for parallel loop
% disp(['iseg = ' num2str(iseg) '/' num2str(Nseg)])
   xyNSWE0 = [xNSWE0(:,iseg); yNSWE0(:,iseg)];
tic
         [~, xyNSWE] = ode23(@uv, ...
            [tini tend], xyNSWE0, ...
                options, ...
           u, v, xspan, yspan, tspan);     

%    [~, xyNSWE] = ode45(@uv, ...
%             [tini tend], xyNSWE0, ...
%                 options, ...
%            u, v, xspan, yspan, tspan);        


%     xyNSWE = ode3(@uv, ...
%        t, xyNSWE0, ...
%        u, v, xspan, yspan, tspan);

disp(['       ',num2str(toc/60),' min'])
   xNSWE(:,iseg) = xyNSWE(end,1:end/2)';
   yNSWE(:,iseg) = xyNSWE(end,end/2+1:end)';
end
disp(['       Integration for CG tensors took ',num2str(toc(tt)/60),' min'])

xNSWE = xNSWE(:);
yNSWE = yNSWE(:);
xN = xNSWE(1:Nxy0);
xS = xNSWE(Nxy0+1:2*Nxy0);
xW = xNSWE(2*Nxy0+1:3*Nxy0);
xE = xNSWE(3*Nxy0+1:4*Nxy0);
yN = yNSWE(1:Nxy0);
yS = yNSWE(Nxy0+1:2*Nxy0);
yW = yNSWE(2*Nxy0+1:3*Nxy0);
yE = yNSWE(3*Nxy0+1:4*Nxy0);
dxdx0 = (xE - xW) / (2*dx0);
dxdy0 = (xN - xS) / (2*dy0);
dydx0 = (yE - yW) / (2*dx0);
dydy0 = (yN - yS) / (2*dy0);
dxdx0 = reshape(dxdx0, [Ny0 Nx0]);
dxdy0 = reshape(dxdy0, [Ny0 Nx0]);
dydx0 = reshape(dydx0, [Ny0 Nx0]);
dydy0 = reshape(dydy0, [Ny0 Nx0]);
C11 = dxdx0.^2 + dydx0.^2;
C12 = dxdx0.*dxdy0 + dydx0.*dydy0;
C22 = dxdy0.^2 + dydy0.^2;


if nargin>6 %will save data in save_dirr
note=write_note(mfilename('fullpath'),save_dirr,['period of integration (ddmm) ',...
    datestr(t0,'ddmm'),'-',datestr(t0+T,'ddmm')]);

save([save_dirr,filesep,'traj_',datestr(t0,'ddmm'),'-', datestr(t0+T,'ddmm') ,'-xy0'],'x0','y0','note')
save([save_dirr,filesep,'traj_',datestr(t0,'ddmm'),'-', datestr(t0+T,'ddmm') ,'-C11'],'C11','note')
save([save_dirr,filesep,'traj_',datestr(t0,'ddmm'),'-', datestr(t0+T,'ddmm') ,'-C12'],'C12','note')
save([save_dirr,filesep,'traj_',datestr(t0,'ddmm'),'-', datestr(t0+T,'ddmm') ,'-C22'],'C22','note')
disp('     saved variables: x0 y0 C11 C12 C22 note')

end

disp([ '  ========================================================================= ';...
    '  ============== compute_C_par finished ',datestr(now),' ============== ';...
     '  ========================================================================= '])
%--------------------------------------------------------------------------
function out = uv(t, xyDxy, u, v, xspan, yspan, tspan)

n = length(xyDxy)/2;
out = zeros(2*n,1);

x = xyDxy(1:n);
y = xyDxy(n+1:2*n);

x = interp1(xspan, 1:length(xspan), x); %this is original 
y = interp1(yspan, 1:length(yspan), y); % this is sorting positions I think
t = interp1(tspan, 1:length(tspan), t);


t = repmat(t, [n 1]);

% ba_interp3 is available in matlab's file exchange, and may accelerate
% computations, but needs compiling. Try of interp3 below is too slow.
% u = ba_interp3(u, x, y, t, 'cubic');
% v = ba_interp3(v, x, y, t, 'cubic');

u = interp3(u, x, y, t, 'cubic');
v = interp3(v, x, y, t, 'cubic');

out(1:n) = u;
out(n+1:2*n) = v;


function note=write_note(mfile_name,data_directory,optional_string)
% note=write_note(mfile_name,data_directory,optional_string)
% call from program of interest with
% mfile_name=mfilename('fullpath');
% directory='C:/Something'; or if currently in data directory just call.
% The note created will be
% note=['Data created with ',mfile_name,'.m with data in ',directory,' on ',date];
% if exist('optional_string','var')
% note{1}=note;
% and
% note{kk}=optional_string{kk-1};
% end

note={['Data created with ',mfile_name,'.m']; ['saved data in  ',data_directory];['on ',datestr(now)]};


if exist('optional_string','var')

    if ~iscell(optional_string)
   temp={optional_string};
   clear optional_string;
   optional_string=temp;
    end

   note2{1}=note; 
   for kk=2:length(optional_string)+1
   note2{kk}=optional_string{kk-1};%#ok
   end
   note=note2;
end


