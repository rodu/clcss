function t = thompson(m)
%THOMPSON    Thompson's color map.
%
%   See also JET, HSV, GRAY, PINK, COOL, BONE, COPPER, FLAG, 
%   COLORMAP, RGBPLOT, SHADDEN, REDBLUE, RED, BLUE.

t = [ 10  50 120
      15  75 165
      30 110 200
      60 160 240
      80 180 250
     130 210 255
     160 240 255
     200 250 255
     230 255 255
     255 250 220
     255 232 120
     255 192  60
     255 160   0
     255  96   0
     255  50   0
     255  20   0
     192   0   0
     165   0   0]/256;
  
if	nargin && ~isempty(m)
	x = (1:size(t,1)).';
	k = linspace(1, x(end), m)';
	t = interp1(x, t, k);
end  
