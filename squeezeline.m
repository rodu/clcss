function [pxt,pyt] = squeezeline(C11,C12,C22,xi,yi,ArcLength,options)
% SQUEEZELINE Line field intergrator.
% [XS, YS] = SQUEEZELINE(C11,C12,C22, X, Y, SSPAN) 
% computes squeezelines from the Cauchy-Green tensor entries C11, C12 and C22 
% which are a function of (X,Y)
% each column of XS, YS is a squeezeline
% integrates the line field (Lx(X,Y),Ly(X,Y))  from SSPAN(1) to SSPAN(2).
% suggestion: SPAN=[0 150];
% to get squeezelines in lon lat do
% [pxt2,pyt2]=xy2sph(pxt*1e3, min(lon0), pyt*1e3, min(lat0));
% One can choose how to get initial conditions for squeezeline integration
% choose one case in line 43-44
%
%  Alireza Hadjighasem 2018
%  $Revision: 1.1 $  $Date: 20-Dec-2018 13:52:34 $ $Francisco J. Beron-Vera $
% modified by R. Duran -- July-August 2019.
% RD 11/2020 corected a minor error in the initialization of tensforfield
%

% disp([ '   ===================================================================== ';...
%     '   ============== squeezeline began  ',datestr(now),' ============== ';...
%      '   ===================================================================== '])

disp(' ')
disp( ['      =========== squeezeline began   ',datestr(now),' =========== '])
% compute tensor lines and eigenvalues
detC = C11.*C22 - C12.^2;
trC = C11 + C22;
lda1 = real(.5*trC - sqrt(.25*trC.^2 - detC));
lda2 = real(.5*trC + sqrt(.25*trC.^2 - detC));
xi2x = real(-C12./sqrt((C11 - lda2).^2 + C12.^2));
xi2y = real((C11 - lda2)./sqrt((C11 - lda2).^2 + C12.^2));
xi1x = - xi2y;
xi1y = + xi2x;
v = (lda1 - lda2).^2./(lda1 + lda2).^2;
vx = xi1x.*v;
vy = xi1y.*v;
vx(isnan(vx) | isnan(vy)) = 0; % 0's on land
vy(isnan(vx) | isnan(vy)) = 0; % 0's on land
eta_1=vx;
eta_2=vy;

% use following lines to select which type of initial
% conditions for tensorline integration
% regular uses a regular grid
% maxlda2 seeds initial conditions where \lambda_2 is locally max

% casse='regular'; % a regular grid of ICs, adjust the value for Nkm below
casse='maxlda2'; % seed ICs at lambda_2 maxima, this option can be faster
disp(['      Will be using ',casse,' for initial conditions'])
switch casse
    case 'maxlda2'
        % get ICs - within Nxb x Nyb boxes find local lda2 maxima
        Nxb = 25; % boxes in x
        Nyb = 25; % boxes in y
        disp(['      Dividing domain in ',...
            num2str(Nxb),'x',num2str(Nyb),...
            ' boxes for lda2 max search'])
        lda2(isnan(xi2x) | isnan(xi2y)) = NaN; % NaN's on land
        [X0, Y0] = meshgrid(xi, yi);
        X0 = X0(:);
        Y0 = Y0(:);
        xblim = linspace(min(xi), max(xi), Nxb+1); % box limits
        yblim = linspace(min(yi), max(yi), Nyb+1); % box limits
        xs0 = [];
        ys0 = [];
        for ixb = 1:Nxb
           for iyb = 1:Nyb
              lda2b = lda2;
              Ixb = xi < xblim(ixb) | xi > xblim(ixb+1);
              Iyb = yi < yblim(iyb) | yi > yblim(iyb+1);
              lda2b(Iyb,:) = NaN;
              lda2b(:,Ixb) = NaN;
              [lda2bIb, Ib] = max(lda2b(:));
              if ~isnan(lda2bIb)
                 xs0 = [xs0; X0(Ib)];
                 ys0 = [ys0; Y0(Ib)];
              end
           end
        end
        x0=xs0;
        y0=ys0;


case 'regular' 
        % get ICs option 2, use a regular grid
        minx=min(xi(:));
        maxx=max(xi(:));
        miny=min(yi(:));
        maxy=max(yi(:));
        Nkm=40; % get a squeezeline every Nkm; modify this if you like.
        Nx=round((maxx-minx)/Nkm);
        Ny=round((maxy-miny)/Nkm);
%       Nkmx=27; % get a squeezeline every N km; modify this if you like.
%       Nkmy=40; % get a squeezeline every N km; modify this if you like.
%       Nx=round((maxx-minx)/Nkmx);
%       Ny=round((maxy-miny)/Nkmy);
        % % or override with a fixed number, 
        % % Nx and Ny about 25, work ok for GoM but it misses some maxima
        % Nx=30;
        % Ny=30;
        x0=linspace(minx,maxx,Nx);
        y0=linspace(miny,maxy,Ny);
        [x0,y0]=meshgrid(x0,y0);
end

% % check what just happened
%         figure
%         pcolor(xi,yi,log(lda2)), caxis([0 5]), colormap shadden
%         hold on
%         plot(x0,y0,'k.','MarkerSize',12)

if nargin < 7
	options = odeset('RelTol', 1e-6, 'AbsTol', 1e-6);
end

if sum(size(xi) - size(eta_1)) ~= 0 
	[xi, yi] = meshgrid(xi, yi);
end
x0 = x0(:);
y0 = y0(:);
[m,n] = size(xi);
Np = numel(x0);
nn=0;
x = xi(1,:)';
dx = abs( x(2)-x(1) );
y = yi(:,1);
dy = abs( y(2)-y(1) );

Xmin = min(x);
Ymin = min(y);
Xmax = max(x);
Ymax = max(y);

% % -- Constructing eta vector field
eta_1( imag(eta_1)~=0 ) = NaN;
eta_2( imag(eta_2)~=0 ) = NaN;

[xi1_1b,xi1_2b] = SmoothVectorField(x0,y0);
%-- make sure that all tensor lines will launch in the same direction
nonull=find( xi1_1b~=0 &  xi1_2b~=0, 1, 'first');
sgn_0  = sign( xi1_1b(nonull).*xi1_1b+xi1_2b(nonull).*xi1_2b );
xi1_1b = sgn_0.*xi1_1b;   
xi1_2b = sgn_0.*xi1_2b;

tic
[~,F] = ode45(@fun,ArcLength(1):ArcLength(2),[x0(:);y0(:)],options);% this is slightly faster than next option
% [~,F] = ode45(@fun,ArcLength,[x0(:);y0(:)],options);
%F = ode4(@fun,ArcLength,[x0(:);y0(:)]);
disp(['      squeezeline integration took ',num2str(toc/60),' min'])

pxt = F(:,1:end/2);
pyt = F(:,end/2+1:end);

% disp([ '   ===================================================================== ';...
%     '   ============== squeezeline ended  ',datestr(now),' ============== ';...
%      '   ===================================================================== '])


disp(['      =========== squeezeline ended   ',datestr(now),' =========== '])
disp(' ')
%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
	function dy = fun(~,y)
		nn=nn+1;
		%- Freez the particles at the boundary
		y(1:Np,1)      = min( y(1:Np,1)      ,Xmax );
		y(1:Np,1)      = max( y(1:Np,1)      ,Xmin );
		y(Np+1:2*Np,1) = min( y(Np+1:2*Np,1) ,Ymax );
		y(Np+1:2*Np,1) = max( y(Np+1:2*Np,1) ,Ymin );
		
		[xi1_1,xi1_2] = SmoothVectorField(y(1:Np),y(Np+1:2*Np));
		
		sgn_2 = sign( xi1_1.*xi1_1b+xi1_2.*xi1_2b );
		
		xi1_1 = sgn_2.*xi1_1;
		xi1_2 = sgn_2.*xi1_2;
		
		
		dy = zeros(2*Np,1);     % a column vector
		dy(1:Np)      = xi1_1;
		dy(Np+1:2*Np) = xi1_2;
		
		dy(isnan(dy)) = 0;
		
		xi1_1b = xi1_1;
		xi1_2b = xi1_2;
	end
%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
	function [v1_0,v2_0] = SmoothVectorField(x0,y0)
		%-- Get indices for 4 neighbors
		id1_UL = floor( (y0-Ymin)/dy ) + 1;
		id2_UL = floor( (x0-Xmin)/dx ) + 1;
		ind_UL = safe_sub2ind([m,n],id1_UL,id2_UL);
		
		id1_UR = id1_UL;
		id2_UR = id2_UL+1;
		ind_UR = safe_sub2ind([m,n],id1_UR,id2_UR);
		
		id1_DL = id1_UL+1;
		id2_DL = id2_UL;
		ind_DL = safe_sub2ind([m,n],id1_DL,id2_DL);
		
		id1_DR = id1_UL+1;
		id2_DR = id2_UL+1;
		ind_DR = safe_sub2ind([m,n],id1_DR,id2_DR);
		
		%-- Get vector field values for 4 neighbors
		v1_UL = eta_1(ind_UL);
		v1_UR = eta_1(ind_UR);
		v1_DL = eta_1(ind_DL);
		v1_DR = eta_1(ind_DR);
		
		v2_UL = eta_2(ind_UL);
		v2_UR = eta_2(ind_UR);
		v2_DL = eta_2(ind_DL);
		v2_DR = eta_2(ind_DR);
		
		%-- Local Smoothing
		sgn_1 = sign( v1_UL.*v1_UR + v2_UL.*v2_UR );
		v1_UR = sgn_1.*v1_UR;
		v2_UR = sgn_1.*v2_UR;
		
		sgn_1 = sign( v1_UL.*v1_DL + v2_UL.*v2_DL );
		v1_DL = sgn_1.*v1_DL;
		v2_DL = sgn_1.*v2_DL;
		
		sgn_1 = sign( v1_UL.*v1_DR + v2_UL.*v2_DR );
		v1_DR = sgn_1.*v1_DR;
		v2_DR = sgn_1.*v2_DR;
		
		%-- Bilinear interpolation
		% Bilinear interpolation for v1
		c1 = ( xi(ind_UR)-x0 )/dx;
		c2 = ( x0-xi(ind_UL) )/dx;
		c3 = ( yi(ind_DL)-y0 )/dy;
		c4 = ( y0-yi(ind_UL) )/dy;
		
		v1_0 = c3.*( c1.*v1_UL + c2.*v1_UR ) + c4.*( c1.*v1_DL + c2.*v1_DR );
		
		% Bilinear interpolation for v2
		c1 = ( xi(ind_UR)-x0 )/dx;
		c2 = ( x0-xi(ind_UL) )/dx;
		c3 = ( yi(ind_DL)-y0 )/dy;
		c4 = ( y0-yi(ind_UL) )/dy;
		
		v2_0 = c3.*( c1.*v2_UL + c2.*v2_UR ) + c4.*( c1.*v2_DL + c2.*v2_DR );
		
		%-- Normalizing v
		norm_v = sqrt( v1_0.^2+v2_0.^2 );
		v1_0 = v1_0./(norm_v+(norm_v==0));
		v2_0 = v2_0./(norm_v+(norm_v==0));
		
		%         if any(isnan(v1_0)) || any(isnan(v1_0))
		%             error('... NaN values are detected in %d iteration',nn);
		%         end
	end
end

function ind = safe_sub2ind(sz, rr, cc)
rr(rr < 1) = 1;
rr(rr > sz(1)) = sz(1);
cc(cc < 1) = 1;
cc(cc > sz(2)) = sz(2);
ind = sub2ind(sz, rr, cc);
end