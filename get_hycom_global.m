% get_hycom_global
% this program downloads a HyCOM global reanalysis (22 years; 1994--2015)
% and saves each year's velocity in a mat file named
% hycom_global_yyyy where yyyy is the year
% The example below, with a 10 Gbps conection, it takes about 25 minutes to download each year
% and each file is about 510 Mb. Each velocity component has dimensions of 851x301x361
% Make sure to choose your region of interest by defining model_bbox as
% model_bbox=[minLon maxLon minLat maxLat];
% see also get_any_hycom_data
data_url1='http://tds.hycom.org/thredds/dodsC/GLBv0.08/expt_53.X/data/';
variable={'water_u','water_v'};
depth=0;% sea surface velocity
model_bbox=[-98 -30 7 31];
savename='hycom_global';
temp_stride=8;% =24/3. Vel. is 3-hourly I want one field every 24 hours, so pick one file from every 8.
numdays=inf;% use inf if you want all otherwise use numdays

for yeari=1994:2015
    yearstr=num2str(yeari);
start=[yearstr,'-','01','-','01'];
data_url=[data_url1,yearstr];
get_any_hycom_data(data_url,variable,start,depth,model_bbox,temp_stride,numdays,savename)

end

% test results
clearvars
load('hycom_global_1994.mat')
figure
[lon,lat]=meshgrid(lon,lat);
pcolor(lon.',lat.',sqrt(data.water_u(:,:,end).^2+data.water_v(:,:,end).^2))
shading flat
axis image
colorbar
title('Magnitude for last day (m/s) in 1994 time series')